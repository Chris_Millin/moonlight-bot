﻿using MoonLight.Bot;
using MoonLight.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight
{
    class Program
    {
        private static string commandLine = "-token=<t> -admin=<a> [-timeout=<t>] [-name=<n>] [-debug=<d>] [-help]";
        private static string help = "  -token: The discord bot token https://discordapp.com/login?redirect_to=/developers/applications/me \n" +
            "   -admin: The admin user ID (long number) \n" +
            "   -timeout: Optional Time it takes for a random disconnection to reconnect in ms \n" +
            "   -name: Optional bot name \n" +
            "   -debug: The server Id to log errors to \n" + 
            "   -help: will show this message \n";

        private static CommandLineArgs cmdArgs = new CommandLineArgs(commandLine, help);

        static int Main(string[] args)
        {
            if (!cmdArgs.IsMatch(args))
            {
                Logging.LogError(LogType.Bot, "Command Line Arguments " + string.Join(" ", args) + "\nAre Invalid.");
                return 1;
            }

            ListDictionary arguments = cmdArgs.Match(args);
            if(arguments.Contains("-help"))
            {
                Console.WriteLine(cmdArgs.Help);
                return 0;
            }

            DiscordHost.Instance.Initalise(arguments);

            while (WaitAnyKey())
            {
                System.Threading.Thread.Sleep(5000);
            }

            return 0;
        }

        public static bool WaitAnyKey()
        {
            Logging.LogInfo(LogType.Bot, "Type 'exit' to quit...");
            string text = Console.ReadLine();
            if (text.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
                return true;

            return false;
        }
    }
}
