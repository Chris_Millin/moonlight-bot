﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D1DroidEngine.Helper
{
    public static class EventArgsHelper
    {
        public static bool TryCast<T>(this EventArgs e, out T casted)
        {
            try
            {
                casted = (T)Convert.ChangeType(e, typeof(T));
                return true;
            }
            catch
            {
                casted = default(T);
                return false;
            }
        }

    }
}
