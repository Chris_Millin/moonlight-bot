﻿using Discord;
using Discord.Audio;
using MoonLight.Bot;
using MoonLight.Helpers.Audio;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using VideoLibrary;

namespace MoonLight.Helpers.Audio
{
    public enum MusicType
    {
        Radio,
        Normal,
        Local,
        Soundcloud
    }

    public enum StreamState
    {
        Resolving,
        Queued,
        Playing,
        Completed
    }

    public class AudioPlayer : IDisposable
    {
        #region Public Members

        public Playlist ActivePlaylist
        {
            get
            { return m_activePlaylist; }
        }

        public bool Repeat
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.Repeat;
                return false;
            }
            set
            {
                if (m_activePlaylist != null) m_activePlaylist.Repeat = value;
            }
        }

        public bool ShuffleLoad
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.ShuffleLoad;
                return false;
            }
            set
            {
                if (m_activePlaylist != null) m_activePlaylist.ShuffleLoad = value;
            }
        }

        public bool Pause
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.Paused;
                return false;
            }
            set
            {
                if (m_activePlaylist != null) m_activePlaylist.Paused = value;
            }
        }

        public string LastSong
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.PreviousSong;
                return "Playlist not active";
            }
        }

        public string CurrentSong
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.CurrentSong;
                return "Playlist not active";
            }
        }

        public string NextSong
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.NextSong;
                return "Playlist not active";
            }
        }

        public float Volume
        {
            get
            {
                if (m_activePlaylist != null) return m_activePlaylist.Volume;
                return 0.0f;
            }
            set
            {
                if (m_activePlaylist != null) m_activePlaylist.Volume = value;
            }
        }

        #endregion

        #region Private Members

        private Playlist m_activePlaylist; 
        private Dictionary<string, Playlist> m_avalaiblePlaylists = new Dictionary<string, Playlist>();

        private IAudioClient m_audioClient;
        private DiscordServer m_server;
        private Thread m_playbackThread;

        private bool m_destroyed = false;

        private bool m_loadingPlaylists = false;
        private bool m_loadedPlaylists = false;

        private object m_mutex = new object();

        private CancellationTokenSource m_songCancellationSource = new CancellationTokenSource();

        #endregion

        public AudioPlayer(DiscordServer server)
        {
            m_avalaiblePlaylists.Add(Playlist.DefaultPlaylist, new Playlist(server));
            m_server = server;
            m_playbackThread = new Thread(async () => await PlaybackThread());
            m_playbackThread.Start();
        }

        #region Public Methods

        #region Connection
        public bool ConnectToVoice(string channelName = "", Channel channelLog = null)
        {
            if (m_loadedPlaylists == false && m_loadingPlaylists == false)
            {
                Utils.Execute(async () => await LoadPlaylists(channelLog));
            }

            if(m_server.ConnectToVoice(channelName))
            {
                m_audioClient = m_server.AudioClient;
                if (m_audioClient == null)
                {
                    channelLog?.SendMessage("Failed to connect to channel " + channelName);
                    Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
                    return false;
                }

                channelLog?.SendMessage("Connected to channel " + channelName);
                return true;
            }

            channelLog?.SendMessage("Failed to connect to channel " + channelName);
            Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
            return false;
        }

        public bool ChangeChannel(string channelName, Channel channelLog = null)
        {
            if (m_loadedPlaylists == false && m_loadingPlaylists == false)
            {
                Utils.Execute(async () => await LoadPlaylists(channelLog));
            }

            if (channelName.Equals(m_audioClient.Channel.Name))
            {
                return ReconnectToVoice(channelLog);
            }
            else
            {
                if(m_server.ChangeVoiceChannel(channelName))
                {
                    m_audioClient = m_server.AudioClient;
                    if (m_audioClient == null)
                    {
                        channelLog?.SendMessage("Failed to connect to channel " + channelName);
                        Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
                        return false;
                    }

                    channelLog?.SendMessage("Changed to channel " + channelName);
                    return true;
                }
            }

            channelLog?.SendMessage("Failed to connect to channel " + channelName);
            Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
            return false;
        }

        public bool ReconnectToVoice(Channel channelLog = null)
        {
            if (m_loadedPlaylists == false && m_loadingPlaylists == false)
            {
                Utils.Execute(async () => await LoadPlaylists(channelLog));
            }

            if(m_audioClient != null)
            {
                channelLog?.SendMessage("Trying to connect was a null voice client try just connecting first.");
                return false;
            }

            if (m_server.ChangeVoiceChannel(m_audioClient.Channel.Name))
            {
                m_audioClient = m_server.AudioClient;
                if (m_audioClient == null)
                {
                    channelLog?.SendMessage("Failed to connect to channel " + m_audioClient.Channel.Name);
                    Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
                    return false;
                }

                channelLog?.SendMessage("Reconnected to channel");
                return true;
            }

            channelLog?.SendMessage("Failed to connect to channel " + m_audioClient.Channel.Name);
            Logging.LogError(LogType.Bot, $"Audio client is not connected on server {m_server.Name}.");
            return false;
        }

        public bool DisconnectFromVoice(Channel channelLog = null)
        {
            if(m_server.DisconnectFromVoice())
            {
                channelLog?.SendMessage("Disconnected from voice channel.");
                m_audioClient = null;
                return true;
            }

            channelLog?.SendMessage("Failed to disconnect. Bot may already be disconnected ");
            Logging.LogError(LogType.Bot, $"Failed to disconnect. Bot may already be disconnected on server {m_server.Name}.");
            return false;
        }
        #endregion

        #region Playlist Management

        public bool NewPlaylist(string name, Channel channelLog = null)
        {
            if(!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (!m_avalaiblePlaylists.ContainsKey(name))
            {
                Playlist newPlaylist = new Playlist(name, m_server);
                m_avalaiblePlaylists.Add(name, newPlaylist);
                channelLog?.SendMessage($"New playlist created named {name}.");
                ChangePlaylist(name);
                return true;
            }

            channelLog?.SendMessage($"Playlist with the name {name} already exists.");
            return false;
        }

        public bool ChangePlaylist(string name, Channel channelLog = null)
        {
            if (string.IsNullOrWhiteSpace(name))
                name = Playlist.DefaultPlaylist;

            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_avalaiblePlaylists.ContainsKey(name))
            {
                m_activePlaylist?.Stop();
                m_activePlaylist = m_avalaiblePlaylists[name];
                m_activePlaylist.Paused = false;
                channelLog?.SendMessage($"Changed playlist to {name}.");
                return true;
            }

            channelLog?.SendMessage($"No playlist found with the name {name}.");
            return false;
        }

        public bool DeletePlaylist(string name, Channel channelLog = null)
        {
            if(name == Playlist.DefaultPlaylist)
            {
                channelLog?.SendMessage("Cant delete default playlist");
                return false;
            }

            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_avalaiblePlaylists.ContainsKey(name))
            {
                Playlist pl = m_avalaiblePlaylists[name];
                if (pl == m_activePlaylist)
                {
                    m_activePlaylist.Stop();
                    m_activePlaylist = null;
                }

                string fileLoc = pl.PlaylistFile;
                if (File.Exists(fileLoc))
                    File.Delete(fileLoc);

                m_avalaiblePlaylists.Remove(name);

                channelLog?.SendMessage($"Removed Playlist {name}");
                return true;
            }

            channelLog?.SendMessage($"No playlist named {name} to remove.");
            return false;
        }

        public string GetPlaylistInfo()
        {
            if (!m_loadedPlaylists)
            {
                return "Playlists still loading please wait...";
            }

            StringBuilder message = new StringBuilder();
            message.AppendLine($"Playlists For Server {m_server.Name}");
            message.AppendLine($"-------------------------------------------");
            message.AppendLine("Note: The Default playlist is a non saving empty playlist.\nIf you want to just play a random song every now and again use this playlist.");
            message.AppendLine($"-------------------------------------------");
            if (m_activePlaylist != null)
                message.AppendLine($"Current Playlist: {m_activePlaylist.Name}\nCurrent Song: {m_activePlaylist.CurrentSong}\nNext Song: {m_activePlaylist.NextSong}.");
            else
                message.AppendLine("No active playlist running");

            foreach(var playlist in m_avalaiblePlaylists)
            {
                message.AppendLine($"Name: {playlist.Key} Song Count {playlist.Value.Songs}");
            }

            return message.ToString();
        }

        public string GetPlaylistInfo(string name)
        {
            if (!m_loadedPlaylists)
            {
                return "Playlists still loading please wait...";
            }

            if (m_avalaiblePlaylists.ContainsKey(name))
            {
                return m_avalaiblePlaylists[name].GetInfo();
            }

            return $"No Playlist with the name {name}";
        }

        #endregion

        #region Playlist Control

        public bool AddSong(string location, MusicType musicType = MusicType.Normal, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (string.IsNullOrWhiteSpace(location) || location.Length < 3)
            {
                channelLog?.SendMessage("Location is blank or invalid so cant play.");
                Logging.LogError(LogType.Bot, "Location is invalid for the song");
                return false;
            }

            if(m_activePlaylist == null)
            {
                channelLog?.SendMessage("No playlist active so nothing to queue into.");
                Logging.LogError(LogType.Bot, "No playlist active so nothing to queue into.");
                return false;
            }

            Execute(async () =>
            {
                try
                {
                    var resolvedSongs = await ResolveSong(location, musicType).ConfigureAwait(false);
                    bool sendMsg = resolvedSongs?.Count() == 1;
                    foreach (var song in resolvedSongs)
                    {
                        lock (m_mutex)
                        {
                            int queueLoc = m_activePlaylist.AddSong(song);
                            if (queueLoc == -1)
                            {
                                if(sendMsg)
                                    channelLog?.SendMessage("Failed to add song to playlist may already be in playlist or is an invalid link");
                            }
                            else
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage($"Added Song {song.Name} currently {queueLoc} in queue.");
                            }
                        }
                    }

                    if (resolvedSongs?.Count() > 1)
                        channelLog?.SendMessage($"Added  {resolvedSongs.Count()} Songs to queue.");
                }
                catch (Exception ex)
                {
                    channelLog?.SendMessage("Failed to add song to playlist could not resolve request.");
                    Logging.LogException(LogType.Bot, ex, "Exception occured in audio player while trying to resolve and queue song");
                }
            });

            return true;
        }

        public bool AddNextSong(string location, MusicType musicType = MusicType.Normal, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (string.IsNullOrWhiteSpace(location) || location.Length < 3)
            {
                channelLog?.SendMessage("Location is blank or invalid so cant play.");
                Logging.LogError(LogType.Bot, "Location is invalid for the song");
                return false;
            }

            if (m_activePlaylist == null)
            {
                channelLog?.SendMessage("No playlist active so nothing to queue into.");
                Logging.LogError(LogType.Bot, "No playlist active so nothing to queue into.");
                return false;
            }

            Execute(async () =>
            {
                try
                {
                    var resolvedSongs = await ResolveSong(location, musicType).ConfigureAwait(false);
                    bool sendMsg = resolvedSongs.Count() == 1;
                    foreach (var song in resolvedSongs)
                    {
                        lock (m_mutex)
                        {
                            bool success = m_activePlaylist.AddSongNext(song);
                            if (!success)
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage("Failed to add song to playlist may already be in playlist or is an invalid link");
                            }
                            else
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage($"Added Song {song.Name} currently next in queue.");
                            }
                        }
                    }

                    if (resolvedSongs.Count() > 1)
                        channelLog?.SendMessage($"Added  {resolvedSongs.Count()} Songs to queue.");
                }
                catch (Exception ex)
                {
                    channelLog?.SendMessage("Failed to add song to playlist could not resolve request.");
                    Logging.LogException(LogType.Bot, ex, "Exception occured in audio player while trying to resolve and queue song");
                }
            });

            return true;
        }

        public bool AddSong(string playlist, string location, MusicType musicType = MusicType.Normal, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (string.IsNullOrWhiteSpace(location) || location.Length < 3)
            {
                channelLog?.SendMessage("Location is blank or invalid so cant play.");
                Logging.LogError(LogType.Bot, "Location is invalid for the song");
                return false;
            }

            if (!m_avalaiblePlaylists.ContainsKey(playlist))
            {
                channelLog?.SendMessage("Playlist not found so cant add to queue.");
                Logging.LogError(LogType.Bot, "Playlist not found so cant add to queue.");
                return false;
            }

            Execute(async () =>
            {
                try
                {
                    var resolvedSongs = await ResolveSong(location, musicType).ConfigureAwait(false);
                    bool sendMsg = resolvedSongs.Count() == 1;
                    foreach (var song in resolvedSongs)
                    {
                        lock (m_mutex)
                        {
                            int queueLoc = m_avalaiblePlaylists[playlist].AddSong(song);
                            if (queueLoc == -1)
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage("Failed to add song to playlist may already be in playlist or is an invalid link");
                            }
                            else
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage($"Added Song {song.Name} currently {queueLoc} in queue.");
                            }
                        }
                    }

                    if (resolvedSongs.Count() > 1)
                        channelLog?.SendMessage($"Added  {resolvedSongs.Count()} Songs to queue.");
                }
                catch (Exception ex)
                {
                    channelLog?.SendMessage("Failed to add song to playlist could not resolve request.");
                    Logging.LogException(LogType.Bot, ex, "Exception occured in audio player while trying to resolve and queue song");
                }
            });

            return true;
        }

        public bool AddNextSong(string playlist, string location, MusicType musicType = MusicType.Normal, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (string.IsNullOrWhiteSpace(location) || location.Length < 3)
            {
                channelLog?.SendMessage("Location is blank or invalid so cant play.");
                Logging.LogError(LogType.Bot, "Location is invalid for the song");
                return false;
            }

            if (!m_avalaiblePlaylists.ContainsKey(playlist))
            {
                channelLog?.SendMessage("Playlist not found so cant add to queue.");
                Logging.LogError(LogType.Bot, "Playlist not found so cant add to queue.");
                return false;
            }

            Execute(async () =>
            {
                try
                {
                    var resolvedSongs = await ResolveSong(location, musicType).ConfigureAwait(false);
                    bool sendMsg = resolvedSongs.Count() == 1;
                    foreach (var song in resolvedSongs)
                    {
                        lock (m_mutex)
                        {
                            bool success = m_avalaiblePlaylists[playlist].AddSongNext(song);
                            if (!success)
                            {
                                if(sendMsg)
                                    channelLog?.SendMessage("Failed to add song to playlist may already be in playlist or is an invalid link");
                            }
                            else
                            {
                                if (sendMsg)
                                    channelLog?.SendMessage($"Added Song {song.Name} currently next in queue.");
                            }
                        }
                    }

                    if(resolvedSongs.Count() > 1)
                        channelLog?.SendMessage($"Added  {resolvedSongs.Count()} Songs to queue.");

                }
                catch (Exception ex)
                {
                    channelLog?.SendMessage("Failed to add song to playlist could not resolve request.");
                    Logging.LogException(LogType.Bot, ex, "Exception occured in audio player while trying to resolve and queue song");
                }
            });

            return true;
        }

        public bool SetNext(string name, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_activePlaylist != null)
                return m_activePlaylist.SetNext(name);
            else
                return false;
        }

        public bool Requeue(Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_activePlaylist != null)
            {
                m_activePlaylist.RequeueAll();
                return true;
            }
            return false;
        }

        public bool Skip()
        {
            if (!m_loadedPlaylists)
            {
                return false;
            }

            if (m_activePlaylist != null)
            {
                return m_activePlaylist.Skip();
            }

            return false;
        }

        public bool Remove(string name, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_activePlaylist != null)
            {
                return m_activePlaylist.RemoveSong(name);
            }

            return false;
        }

        public bool Clear(bool all = false, Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return false;
            }

            if (m_activePlaylist != null)
            {
                if (all)
                    return m_activePlaylist.Clear();
                else
                    return m_activePlaylist.ClearPlaying();
            }

            return false;
        }

        public void Shuffle(Channel channelLog = null)
        {
            if (!m_loadedPlaylists)
            {
                channelLog?.SendMessage("Playlists still loading please wait...");
                return;
            }

            if (m_activePlaylist != null)
            {
                m_activePlaylist.Shuffle();
                channelLog?.SendMessage("Playlist shuffled");
            }
        }

        public void Stop()
        {
            if (!m_loadedPlaylists)
            {
                return;
            }

            if (m_activePlaylist != null)
            {
                m_activePlaylist.Stop();
            }
        }

        #endregion

        public void Dispose()
        {
            Stop();
            m_destroyed = true;
            bool success = Utils.ExecuteAndWait(async (token) =>
            {
                while (m_playbackThread.IsAlive)
                {
                    await Task.Delay(500);
                    if (token.IsCancellationRequested)
                        break;
                }
            }, 10000);

            if (!success)
            {
                Logging.LogWarn(LogType.Bot, "Audio Playback thread failed to exit in requested timeout so will be aborted.");
                m_playbackThread.Abort();
            }
            else
            {
                Logging.LogInfo(LogType.Bot, "Audio Playback thread successfully aborted");
            }
        }

        #endregion

        #region Private Methods

        private Task LoadPlaylists(Channel channel = null)
        {
            return Task.Factory.StartNew(() =>
            {
                m_loadingPlaylists = true;

                channel?.SendMessage("Building Playlists please wait...");
                if (m_server.Id != 1 || m_server.Id == 0)
                {
                    string dir = Path.Combine(Utils.DataFolder, m_server.Id.ToString(), "Playlists");
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    foreach (var playlistXML in Directory.EnumerateFiles(dir, "*.xml"))
                    {
                        Playlist pl = new Playlist(Path.GetFileNameWithoutExtension(playlistXML), m_server);
                        if (pl.IsValid)
                            m_avalaiblePlaylists.Add(pl.Name, pl);
                        else
                            channel?.SendMessage($"Failed to add playlist {pl.Name}");
                    }

                    channel?.SendMessage("Building playlists is complete");
                    m_activePlaylist = m_avalaiblePlaylists[Playlist.DefaultPlaylist];
                    m_loadedPlaylists = true;
                    m_loadingPlaylists = false;
                }
            });
        }

        private void Execute(Func<Task> asyncAction)
        {
            asyncAction();
        }

        private void ExecuteAndWait(Func<Task> asyncAction)
        {
            asyncAction().GetAwaiter().GetResult();
        }

        private async Task PlaybackThread()
        {
            while (!m_destroyed)
            {
                try
                {
                    if (m_activePlaylist != null)
                        await m_activePlaylist.Play(m_audioClient);
                    else
                        await Task.Delay(500).ConfigureAwait(false);
                }
                catch(Exception ex)
                {
                    Logging.LogException(LogType.Bot, ex, $"Failed to play song {m_activePlaylist.CurrentSong} on Audio Play for server {m_server.Name}.");
                }
            }
        }

        #endregion

        #region Static Helpers

        public static async Task<IEnumerable<Song>> ResolveSong(string query, MusicType musicType = MusicType.Normal)
        {
            List<Song> m_songs = new List<Song>();
            
            if (string.IsNullOrWhiteSpace(query))
                throw new ArgumentNullException(nameof(query));

            if (musicType != MusicType.Local && IsRadioLink(query))
            {
                musicType = MusicType.Radio;
                query = await HandleStreamContainers(query).ConfigureAwait(false) ?? query;
            }

            try
            {
                switch (musicType)
                {
                    case MusicType.Local:
                        m_songs.Add(new Song(new SongInfo
                        {
                            Uri = "\"" + Path.GetFullPath(query) + "\"",
                            Title = Path.GetFileNameWithoutExtension(query),
                            Provider = "Local File",
                            ProviderType = musicType,
                            Query = query,
                        }));
                        return m_songs;
                    case MusicType.Radio:
                        m_songs.Add(new Song(new SongInfo
                        {
                            Uri = query,
                            Title = $"{query}",
                            Provider = "Radio Stream",
                            ProviderType = musicType,
                            Query = query
                        }));
                        return m_songs;
                }
                if (WebRequester.Instance.IsSoundCloudLink(query))
                {
                    var svideo = await WebRequester.Instance.ResolveVideoAsync(query).ConfigureAwait(false);
                    m_songs.Add(new Song(new SongInfo
                    {
                        Title = svideo.FullName,
                        Provider = "SoundCloud",
                        Uri = svideo.StreamLink,
                        ProviderType = musicType,
                        Query = svideo.TrackLink,
                    }));
                    return m_songs;
                }

                if (musicType == MusicType.Soundcloud)
                {
                    var svideo = await WebRequester.Instance.GetVideoByQueryAsync(query).ConfigureAwait(false);
                    m_songs.Add(new Song(new SongInfo
                    {
                        Title = svideo.FullName,
                        Provider = "SoundCloud",
                        Uri = svideo.StreamLink,
                        ProviderType = MusicType.Normal,
                        Query = svideo.TrackLink,
                    }));
                    return m_songs;
                }

                var links = await WebRequester.Instance.FindYoutubeUrlByKeywords(query).ConfigureAwait(false);
                foreach (var link in links)
                {
                    if (string.IsNullOrWhiteSpace(link))
                        throw new OperationCanceledException("Not a valid youtube query.");
                    var allVideos = await Task.Factory.StartNew(async () => await YouTube.Default.GetAllVideosAsync(link).ConfigureAwait(false)).Unwrap().ConfigureAwait(false);
                    var videos = allVideos.Where(v => v.AdaptiveKind == AdaptiveKind.Audio);

                    YouTubeVideo video = null;
                    try
                    {
                        video = videos
                            .Where(v => v.AudioBitrate < 192)
                            .OrderByDescending(v => v.AudioBitrate)
                            .FirstOrDefault();
                    }
                    catch { }

                    if (video == null) // do something with this error
                    {
                        continue;
                        //throw new Exception("Could not load any video elements based on the query.");
                    }

                    var m = Regex.Match(query, @"\?t=((?<h>\d*)h)?((?<m>\d*)m)?((?<s>\d*)s?)?");
                    int gotoTime = 0;
                    if (m.Captures.Count > 0)
                    {
                        int hours;
                        int minutes;
                        int seconds;

                        int.TryParse(m.Groups["h"].ToString(), out hours);
                        int.TryParse(m.Groups["m"].ToString(), out minutes);
                        int.TryParse(m.Groups["s"].ToString(), out seconds);

                        gotoTime = hours * 60 * 60 + minutes * 60 + seconds;
                    }

                    m_songs.Add(new Song(new SongInfo
                    {
                        Title = video.Title.Substring(0, video.Title.Length - 10), // removing trailing "- You Tube"
                        Provider = "YouTube",
                        Uri = video.Uri,
                        Query = link,
                        ProviderType = musicType,
                    }));
                    m_songs.Last().SkipTo = gotoTime;
                }
                return m_songs;
            }
            catch (Exception ex)
            {
                Logging.LogException(LogType.Bot, ex, "Failed resolving the link.");
                return null;
            }
        }

        private static async Task<string> HandleStreamContainers(string query)
        {
            string file = null;
            try
            {
                file = await WebRequester.Instance.GetResponseStringAsync(query).ConfigureAwait(false);
            }
            catch
            {
                return query;
            }
            if (query.Contains(".pls"))
            {
                //File1=http://armitunes.com:8000/
                //Regex.Match(query)
                try
                {
                    var m = Regex.Match(file, "File1=(?<url>.*?)\\n");
                    var res = m.Groups["url"]?.ToString();
                    return res?.Trim();
                }
                catch
                {
                    Console.WriteLine($"Failed reading .pls:\n{file}");
                    return null;
                }
            }
            if (query.Contains(".m3u"))
            {
                try
                {
                    var m = Regex.Match(file, "(?<url>^[^#].*)", RegexOptions.Multiline);
                    var res = m.Groups["url"]?.ToString();
                    return res?.Trim();
                }
                catch
                {
                    Console.WriteLine($"Failed reading .m3u:\n{file}");
                    return null;
                }

            }
            if (query.Contains(".asx"))
            {
                //<ref href="http://armitunes.com:8000"/>
                try
                {
                    var m = Regex.Match(file, "<ref href=\"(?<url>.*?)\"");
                    var res = m.Groups["url"]?.ToString();
                    return res?.Trim();
                }
                catch
                {
                    Console.WriteLine($"Failed reading .asx:\n{file}");
                    return null;
                }
            }
            if (query.Contains(".xspf"))
            {
                /*
                <?xml version="1.0" encoding="UTF-8"?>
                    <playlist version="1" xmlns="http://xspf.org/ns/0/">
                        <trackList>
                            <track><location>file:///mp3s/song_1.mp3</location></track>
                */
                try
                {
                    var m = Regex.Match(file, "<location>(?<url>.*?)</location>");
                    var res = m.Groups["url"]?.ToString();
                    return res?.Trim();
                }
                catch
                {
                    Console.WriteLine($"Failed reading .xspf:\n{file}");
                    return null;
                }
            }

            return query;
        }

        private static bool IsRadioLink(string query)
        {
            return (query.StartsWith("http") ||
            query.StartsWith("ww"))
            &&
            (query.Contains(".pls") ||
            query.Contains(".m3u") ||
            query.Contains(".asx") ||
            query.Contains(".xspf"));
        }

        #endregion
    }
}
