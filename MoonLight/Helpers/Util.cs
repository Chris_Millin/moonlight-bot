﻿using Discord;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MoonLight.Helpers
{
    public class ExclusiveSynchronizationContext : SynchronizationContext
    {
        private bool done;
        public Exception InnerException { get; set; }
        readonly AutoResetEvent workItemsWaiting = new AutoResetEvent(false);
        readonly Queue<Tuple<SendOrPostCallback, object>> items =
            new Queue<Tuple<SendOrPostCallback, object>>();

        public override void Send(SendOrPostCallback d, object state)
        {
            throw new NotSupportedException("We cannot send to our same thread");
        }

        public override void Post(SendOrPostCallback d, object state)
        {
            lock (items)
            {
                items.Enqueue(Tuple.Create(d, state));
            }
            workItemsWaiting.Set();
        }

        public void EndMessageLoop()
        {
            Post(_ => done = true, null);
        }

        public void BeginMessageLoop()
        {
            while (!done)
            {
                Tuple<SendOrPostCallback, object> task = null;
                lock (items)
                {
                    if (items.Count > 0)
                    {
                        task = items.Dequeue();
                    }
                }
                if (task != null)
                {
                    task.Item1(task.Item2);
                    if (InnerException != null) // the method threw an exeption
                    {
                        throw new AggregateException("AsyncHelpers.Run method threw an exception.", InnerException);
                    }
                }
                else
                {
                    workItemsWaiting.WaitOne();
                }
            }
        }

        public override SynchronizationContext CreateCopy()
        {
            return this;
        }
    }

    public static class Utils
    {
        public static string DataFolder
        {
            get { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Data"); }
        }

        public static string TempFolder
        {
            get { return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Temp"); }
        }

        public static string ScriptFolder
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), c_scriptFolder);
            }
        }

        public static string GlobalScriptFolder
        {
            get { return Path.Combine(ScriptFolder, c_globalScriptFolder); }
        }

        public static string AdminScriptFolder
        {
            get { return Path.Combine(ScriptFolder, c_adminScriptFolder); }
        }

        private const string c_scriptFolder            = "scripts";
        private const string c_globalScriptFolder      = "global";
        private const string c_adminScriptFolder       = "admin";

        public static bool IsAdminScript(string file)
        {
            if (string.IsNullOrEmpty(file)) return false;
            return file.StartsWith(Utils.AdminScriptFolder);
        }

        public static bool IsGlobalScript(string file)
        {
            if (string.IsNullOrEmpty(file)) return false;
            return file.StartsWith(Utils.GlobalScriptFolder);
        }

        public static bool IsServerScript(string file, ulong server)
        {
            if (string.IsNullOrEmpty(file) || server == 0) return false;
            return file.StartsWith(Path.Combine(ScriptFolder, server.ToString()));
        }

        /// <summary>
        /// Execute's an async Task<T> method which has a void return value synchronously
        /// </summary>
        /// <param name="task">Task<T> method to execute</param>
        public static void ExecuteAndWait(Func<Task> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            synch.Post(async _ =>
            {
                try
                {
                    await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();

            SynchronizationContext.SetSynchronizationContext(oldContext);
        }

        /// <summary>
        /// Execute's an async Task<T> method which has a T return type synchronously
        /// </summary>
        /// <typeparam name="T">Return Type</typeparam>
        /// <param name="task">Task<T> method to execute</param>
        /// <returns></returns>
        public static T ExecuteAndWait<T>(Func<Task<T>> task)
        {
            var oldContext = SynchronizationContext.Current;
            var synch = new ExclusiveSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(synch);
            T ret = default(T);
            synch.Post(async _ =>
            {
                try
                {
                    ret = await task();
                }
                catch (Exception e)
                {
                    synch.InnerException = e;
                    throw;
                }
                finally
                {
                    synch.EndMessageLoop();
                }
            }, null);
            synch.BeginMessageLoop();
            SynchronizationContext.SetSynchronizationContext(oldContext);
            return ret;
        }

        public static void Execute(Func<Task> asyncAction)
        {
            asyncAction();
        }

        public static void Execute(Func<CancellationToken, Task> asyncAction, int timeout)
        {
            Execute(async () =>
            {
                CancellationTokenSource tokenSource = new CancellationTokenSource();
                var task = asyncAction(tokenSource.Token);
                if(await Task.WhenAny(task, Task.Delay(timeout, tokenSource.Token)) == task)
                {
                    await task;
                }
                else
                {
                    tokenSource.Cancel();
                }
            });
        }

        public static bool ExecuteAndWait(Func<CancellationToken, Task> asyncAction, int timeout)
        {
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            var task = asyncAction(tokenSource.Token);
            if(Task.WhenAny(task, Task.Delay(timeout, tokenSource.Token)).GetAwaiter().GetResult() == task)
            {
                task.ConfigureAwait(false).GetAwaiter().GetResult();
                return true;
            }
            else
            {
                tokenSource.Cancel();
                return false;
            }
        }

        public static float Clamp(float min, float max, float value)
        {
            if (value > max) return max;
            if (value < min) return min;
            return value;
        }

        public static bool DownloadFile(string url, string saveLocation)
        {
            using (DebugLogging log = new DebugLogging())
            {
                using (WebClient client = new WebClient())
                {
                    try
                    {
                        client.DownloadFile(new Uri(url), saveLocation);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException(LogType.Bot, ex, $"Failed to download file {url} make sure the file is valid.");
                        return false;
                    }
                }
            }
        }

        public static string ImageToBase64(Image image)
        {
            Bitmap resized = new Bitmap(image, 200, 200);

            string base64 = Convert.ToBase64String(Utils.ImageToByteArray(resized));
            string type = "image/jpeg;base64";
            string req = $"data:{type},{base64}";

            return req;
        }

        public static byte[] ImageToByteArray(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public static List<string> SplitAndWrapString(string text, char split = ' ', int wrapLimit = 1950)
        {
            List<string> wrapped = new List<string>();
            if(text.Length <= wrapLimit)
            {
                wrapped.Add(text);
                return wrapped;
            }

            int currentCharCount = 0;
            string[] words = text.Split(split);
            StringBuilder sb = new StringBuilder();
            foreach (var word in words)
            {
                if (word.Length > wrapLimit)
                {
                    int startIndex = 0;
                    int addedChars = 0;
                    while (addedChars != word.Length)
                    {
                        string cutSection = word.Substring(startIndex, wrapLimit);
                        addedChars += cutSection.Length;
                        wrapped.Add(cutSection);
                        startIndex += cutSection.Length;
                        if ((addedChars + wrapLimit) >= word.Length)
                        {
                            int length = word.Length - startIndex;
                            cutSection = word.Substring(startIndex, length);
                            addedChars += cutSection.Length;
                            wrapped.Add(cutSection);
                        }
                    }
                    currentCharCount = 0;
                }
                else
                {
                    currentCharCount += word.Length + 1;
                    if (currentCharCount >= wrapLimit && sb.Length != 0 && sb.Length <= wrapLimit)
                    {
                        wrapped.Add(sb.ToString());
                        currentCharCount = 0;
                        sb.Clear();
                    }

                    sb.Append(word + split);
                }
            }

            if (currentCharCount > 0)
                wrapped.Add(sb.ToString());

            return wrapped;

        }

    }

}
