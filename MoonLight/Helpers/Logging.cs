﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight
{
    public enum LogType
    {
        Discord,
        Lua,
        Script,
        Server,
        Bot //for anything else

    }

    public enum LogLevel
    {
        Info,
        Warning,
        Error,
        Debug
    }

    public class DebugLogging : IDisposable
    {
        public static Channel DebugChannel
        {
            get
            {
                return m_debugChannel;
            }
            set
            {
                m_debugChannel = value;
                if(m_unloggedErrors.Count > 0 && m_debugChannel != null)
                {
                    foreach (string message in m_unloggedErrors)
                        m_debugChannel.SendMessage(message);
                }
            }
        }

        public static bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;
            }
        }

#if DEBUG
        private static bool m_enabled = true;
#else
        private static bool m_enabled = false;
#endif
        private static Channel m_debugChannel = null;
        private static Queue<string> m_unloggedErrors = new Queue<string>(100);
        private static bool m_hooked = false;
        private bool m_thisHooked = false;
        private static object mutex = new object();


        public DebugLogging()
        {
            lock(mutex)
            {
                if (Enabled && !m_hooked && !m_thisHooked)
                {
                    Logging.OnLog += Logging_OnLog;
                    m_hooked = true;
                    m_thisHooked = true;
                }
            }
        }

        private void Logging_OnLog(LogLevel level, string message)
        {
            if (level == LogLevel.Error)
            {
                if (Enabled && m_debugChannel != null)
                {
                    m_debugChannel.SendMessage(message);
                }
                else if (Enabled && m_debugChannel == null)
                {
                    m_unloggedErrors.Enqueue(message);
                }
            }
        }

        public void Dispose()
        {
            lock (mutex)
            {
                if (Enabled && m_hooked && m_thisHooked)
                {
                    Logging.OnLog -= Logging_OnLog;
                    m_hooked = false;
                }
            }
        }
    }

    public delegate void LogHandler(LogLevel level, string message);

    public class Logging
    {

        public static event LogHandler OnLog;

        private static object m_mutex = new object();

        public static void LogInfo(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Info, message, obj);
        }

        public static void LogWarn(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Warning, message, obj);
        }

        public static void LogError(LogType type, string message, params object[] obj)
        {
            Log(type, LogLevel.Error, message, obj);
        }

        public static void LogInfo(LogType type, string message)
        {
            Log(type, LogLevel.Info, message);
        }

        public static void LogWarn(LogType type, string message)
        {
            Log(type, LogLevel.Warning, message);
        }

        public static void LogError(LogType type, string message)
        {
            Log(type, LogLevel.Error, message);
        }

        public static void LogException(LogType type, Exception ex, string message)
        {
            Log(type, LogLevel.Error, $"{message}\n\n`Exception: {ex.Message}`\n\n```Stack: {ex.StackTrace}\n```");
        }

        public static void LogException(LogType type, LogLevel level, Exception ex, string message)
        {
            Log(type, level, $"{message}\n\n`Exception: {ex.Message}`\n\n```Stack: {ex.StackTrace}\n```");
        }

        public static void Log(LogType type, LogLevel level, string message)
        {
            Log(level, string.Format("[{0}]:[{1}] <{2}> : ", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), type.ToString(), level.ToString()) + message);
        }

        public static void Log(LogType type, LogLevel level, string message, params object[] obj)
        {
            Log(level, string.Format("[{0}]:[{1}] <{2}> : ", DateTime.Now.ToString("dd/MM/yy HH:mm:ss"), type.ToString(), level.ToString()) + string.Format(message, obj));
        }

        private static void Log(LogLevel level, string message)
        {
            lock(m_mutex)
            {
                if (OnLog != null)
                    OnLog(level, message);

                Console.WriteLine(message);
            }
        }
    }
}
