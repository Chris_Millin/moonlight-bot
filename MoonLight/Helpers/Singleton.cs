﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Helpers
{
    public class Singleton<T> where T : class
    {
        private static readonly T _instance = Activator.CreateInstance<T>();

        protected Singleton() {}
        
        public static T Instance
        {
            get
            {
                return _instance;
            }
        }

    }
}
