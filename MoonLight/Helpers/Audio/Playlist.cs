﻿using Discord;
using Discord.Audio;
using MoonLight.Bot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MoonLight.Helpers.Audio
{
    public class Playlist
    {
        #region Public Members

        public string Name
        {
            get { return m_name; }
        }

        public static string DefaultPlaylist
        {
            get { return "Default"; }
        }

        public string PlaylistFile
        {
            get { return m_playlistFile; }
        }

        public bool Paused
        {
            get;
            set;
        }

        public bool Repeat
        {
            get { return m_repeat; }
            set { m_repeat = value; UpdatePlaylist(); }
        }

        public bool ShuffleLoad
        {
            get { return m_shuffleLoad; }
            set
            {
                m_shuffleLoad = value;
                UpdatePlaylist();
            }
        }

        public float Volume
        {
            get { return m_volume; }
            set
            {
                m_volume = Utils.Clamp(0.0f, 1.0f, value);
                UpdatePlaylist();
            }
        }

        public string CurrentSong
        {
            get
            {
                if(m_currentSong != null) return m_currentSong.Name;
                return "No song currently playing";
            }
        }

        public string NextSong
        {
            get
            {
                lock(m_mutex)
                {
                    if (m_activePlaylist.Count >= 1)
                        return m_activePlaylist.First().Name;
                    else
                        return "Playlist Queue Empty";
                }
            }
        }

        public string PreviousSong
        {
            get
            {
                if (m_lastSong != null) return m_lastSong.Name;
                return "Playlist has not completed a song yet";
            }
        }

        public bool IsValid
        {
            get { return m_isValid; }
        }

        public int Songs
        {
            get { return m_playlistCollection.Count; }
        }

        public string ServerOwner
        {
            get { return m_owner.Name; }
        }
        #endregion

        #region Private Members

        private string m_name;
        private DiscordServer m_owner;

        private Song m_lastSong;
        private Song m_currentSong;
        private List<Song> m_playlistCollection = new List<Song>();
        private LinkedList<Song> m_activePlaylist = new LinkedList<Song>();

        private object m_mutex = new object();
        private CancellationTokenSource m_songCancellationSource = new CancellationTokenSource();

        private bool m_repeat = false;
        private bool m_shuffleLoad = false;
        private float m_volume = 0.5f;

        private bool m_saveable = true;
        private bool m_isValid = false;
        private string m_playlistFile;
        private const string c_playlistFolder = "Playlists";

        #endregion

        public Playlist(DiscordServer owner)
        {
            m_owner = owner;
            m_saveable = false;
            m_name = DefaultPlaylist;
        }

        public Playlist(string name, DiscordServer owner)
        {
            m_owner = owner;
            LoadPlaylist(name);
        }

        #region Public Methods

        public async Task Play(IAudioClient audioClient)
        {
            if(Paused)
            {
                return;
            }

            if(audioClient != null && audioClient.State == ConnectionState.Connected && m_activePlaylist.Count != 0)
            {
                lock(m_mutex)
                {
                    m_lastSong = m_currentSong;
                    m_currentSong = m_activePlaylist.First.Value;
                    m_activePlaylist.RemoveFirst();
                    Logging.LogInfo(LogType.Bot, $"Changing song to {m_currentSong.Name} on server {m_owner.Name}");
                }

                if (!WebRequester.Instance.CheckUri(m_currentSong.SongInfo.Uri))
                {
                    Logging.LogInfo(LogType.Bot, $"Changing song to {m_currentSong.Name} has an dead link re-resolving song on server {m_owner.Name}");
                    m_currentSong = (await AudioPlayer.ResolveSong(m_currentSong.SongInfo.Query, m_currentSong.SongInfo.ProviderType)).FirstOrDefault();
                    if (m_currentSong != null)
                    {
                        m_currentSong.SetPlaylist(this);
                        UpdateSong(m_currentSong);
                    }
                    else
                    {
                        Logging.LogInfo(LogType.Bot, $"Song {m_currentSong.Name} failed to re-resolving dead link on server {m_owner.Name}");
                        RemoveSong(m_currentSong.Name);
                        m_currentSong = m_lastSong;
                        return;
                    }
                }

                try
                {
                    if (HasSong(m_currentSong.SongInfo))
                    {
                        await m_currentSong.Play(audioClient, m_songCancellationSource.Token);
                        if (Paused)
                            return;

                        RepeatSong();
                    }
                }
                catch(Exception ex)
                {
                    Logging.LogException(LogType.Bot, ex, $"Exception occured in playlist {Name} for server {m_owner.Name}.");
                }
                finally
                {
                    if (!m_songCancellationSource.Token.IsCancellationRequested)
                    {
                        m_songCancellationSource.Cancel();
                    }

                    m_songCancellationSource = new CancellationTokenSource();
                }
            }

            await Task.Delay(500).ConfigureAwait(false);
        }

        public int AddSong(Song newSong)
        {
            if (string.IsNullOrEmpty(newSong.SongInfo.Uri))
                return -1;

            if(!HasSong(newSong.SongInfo))
            {
                lock (m_mutex)
                {
                    newSong.SetPlaylist(this);
                    m_playlistCollection.Add(newSong);
                    m_activePlaylist.AddLast(newSong);
                }

                UpdatePlaylist();

                Logging.LogInfo(LogType.Bot, $"New Song {newSong.Name} added to playlist {Name} on server {m_owner.Name}.");

                return m_activePlaylist.Count;
            }
            else
            {

                lock (m_mutex)
                {
                    newSong.SetPlaylist(this);
                    m_playlistCollection.Add(newSong);
                    m_activePlaylist.AddLast(newSong);
                }
            }

            Logging.LogInfo(LogType.Bot, $"Failed to add Song {newSong.Name} added to playlist {Name} on server {m_owner.Name} as it already exists.");
            return -1;
        }
        
        public bool AddSongNext(Song newSong)
        {
            if (string.IsNullOrEmpty(newSong.SongInfo.Uri))
                return false;

            if (!HasSong(newSong.SongInfo))
            {
                lock (m_mutex)
                {
                    newSong.SetPlaylist(this);
                    m_playlistCollection.Add(newSong);
                    m_activePlaylist.AddFirst(newSong);
                }
                UpdatePlaylist();

                Logging.LogInfo(LogType.Bot, $"New Song {newSong.Name} added to playlist {Name} on server {m_owner.Name}.");

                return true;
            }
            Logging.LogInfo(LogType.Bot, $"Failed to add Song {newSong.Name} added to playlist {Name} on server {m_owner.Name} as it already exists.");
            return false;
        }

        public bool Skip()
        {
            Logging.LogInfo(LogType.Bot, $"Skipping song {m_currentSong.Name} on server {m_owner.Name}");
            m_songCancellationSource.Cancel();
            m_songCancellationSource = new CancellationTokenSource();
            return true;
        }

        public bool RemoveSong(string name)
        {
            if(HasSong(name))
            {
                Song foundSong = m_playlistCollection.FirstOrDefault(s => s.Name == name);
                m_playlistCollection.Remove(foundSong);
                Logging.LogInfo(LogType.Bot, $"Removed song {foundSong.Name} from server {m_owner.Name}.");
                return true;
            }

            return false;
        }

        public bool SetNext(string name)
        {
            if (HasSong(name))
            {
                lock (m_mutex)
                {
                    Song foundSong = m_playlistCollection.FirstOrDefault(s => s.Name == name);
                    m_activePlaylist.AddFirst(foundSong);
                    Logging.LogInfo(LogType.Bot, $"Set song {foundSong.Name} to play next on server {m_owner.Name}");
                }

                return true;
            }

            return false;
        }

        public bool Stop()
        {
            Logging.LogInfo(LogType.Bot, $"Stopping playlist {Name} on server {m_owner.Name}");
            m_songCancellationSource.Cancel();
            m_songCancellationSource = new CancellationTokenSource();
            Paused = true;
            if (!m_saveable)
                m_playlistCollection.Clear();

            return true;
        }

        public bool ClearPlaying()
        {
            lock(m_mutex)
            {
                Logging.LogInfo(LogType.Bot, $"Clearing the active playlist {Name} on server {m_owner.Name}");
                m_activePlaylist.Clear();
            }

            return true;
        }

        public bool Clear()
        {
            lock (m_mutex)
            {
                Logging.LogInfo(LogType.Bot, $"Clearing entire playlist {Name} on server {m_owner.Name}");
                m_playlistCollection.Clear();
            }

            UpdatePlaylist();

            return true;
        }

        public void Shuffle()
        {
            lock(m_mutex)
            {
                Logging.LogInfo(LogType.Bot, $"Shuffling playlist {Name} on server {m_owner.Name}.");
                m_activePlaylist.Shuffle();
            }
        }

        public void RequeueAll()
        {
            ClearPlaying();

            lock (m_mutex)
            {
                Skip();
                foreach (var song in m_playlistCollection)
                {
                    m_activePlaylist.AddLast(song);
                }

                if (m_shuffleLoad)
                    Shuffle();
            }

            Logging.LogInfo(LogType.Bot, $"Requeueing playlist {Name} on server {m_owner.Name}.");
        }

        public bool HasSong(SongInfo songinfo)
        {
            lock (m_mutex)
            {
                return m_playlistCollection.FirstOrDefault(s => s.SongInfo.Title == songinfo.Title) != null;
            }
        }

        public bool HasSong(string name)
        {
            lock (m_mutex)
            {
                return m_playlistCollection.FirstOrDefault(s => s.SongInfo.Title == name) != null;
            }
        }

        public Song GetSong(string name)
        {
            lock (m_mutex)
            {
                return m_playlistCollection.FirstOrDefault(s => s.SongInfo.Title == name);
            }
        }

        public string GetInfo()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Playlist {Name} Info");
            sb.AppendLine($"-------------------------------------------");
            sb.AppendLine($"Repeat: {Repeat}");
            sb.AppendLine($"Shuffle Load: {ShuffleLoad}");
            sb.AppendLine($"Volume: {Volume}");
            sb.AppendLine($"-------------------------------------------");
            if(m_currentSong != null)
                sb.AppendLine($"Playing: {m_currentSong.Name} - {m_currentSong.Source}");
            if(m_lastSong != null)
                sb.AppendLine($"Last Song: {m_lastSong.Name} - {m_lastSong.Source}");
            if(m_activePlaylist.Count >= 1)
                sb.AppendLine($"Next Song: {m_activePlaylist.First.Value.Name} - {m_activePlaylist.First.Value.Source}");
            sb.AppendLine();

            int id = 1;
            foreach (var song in m_activePlaylist)
            {
                sb.AppendLine($"Song {id++}: {song.Name} - {song.Source}");
            }
            sb.AppendLine();
            return sb.ToString();
        }

        #endregion

        #region Private Methods

        private void RepeatSong()
        {
            if (!Repeat)
                return;

            lock(m_mutex)
            {
                if(m_currentSong != null && HasSong(m_currentSong.SongInfo))
                {
                    m_activePlaylist.AddLast(m_currentSong);
                }
            }
        }

        private void UpdateSong(Song song)
        {
            lock(m_mutex)
            {
                int index = m_playlistCollection.FindIndex(s => s.Name == song.Name);
                if (index != -1)
                {
                    m_playlistCollection[index] = song;
                }
            }

            UpdatePlaylist();
        }



        //<XML Structure>
        /*
            <Playlist>
                <Info Name="name">
                <Songs>
                    <Song ... all the values in SongInfo>
                </Songs>
            </Playlist>

        */

        private void LoadPlaylist(string name)
        {
            if (m_owner == null || string.IsNullOrWhiteSpace(name))
            {
                Logging.LogError(LogType.Bot, $"Server or playlist name is empty ignoring playlist.");
                m_isValid = false;
                return;
            }

            string fileLocation = Path.Combine(Utils.DataFolder, m_owner.Id.ToString(), c_playlistFolder, $"{name}.xml");
            if (!File.Exists(fileLocation))
            {
                CreateNew(name, fileLocation);
                return;
            }

            XDocument doc = XDocument.Load(fileLocation);
            XElement root = doc.Root;
            XElement info = root.Element("Info");
            string xmlName = string.Empty;
            if(info.TryGetAttribute("Name", out xmlName))
            {
                if(xmlName != name)
                {
                    Logging.LogError(LogType.Bot, $"Xml document {fileLocation} has info Name {xmlName} but Name {name} was used for loading playlist in server {m_owner.Name}.");
                    m_isValid = false;
                    return;
                }

                m_name = xmlName;

                bool repeat = false;
                if (info.TryGetAttribute("Repeat", out repeat))
                {
                    m_repeat = repeat;
                }
                bool shuffle = false;
                if (info.TryGetAttribute("Shuffle", out shuffle))
                {
                    m_shuffleLoad = shuffle;
                }
                float volume = 1.0f;
                if (info.TryGetAttribute("Volume", out volume))
                {
                    m_volume = Utils.Clamp(0.0f, 1.0f, volume);
                }

                XElement songs = root.Element("Songs");
                if (songs.HasElements)
                {
                    foreach (var song in songs.Elements("Song"))
                    {
                        SongInfo songInfo = GetSongInfoFromXml(song);
                        if(!string.IsNullOrEmpty(songInfo.Title))
                        {
                            Song addSong = new Song(songInfo);
                            addSong.SetPlaylist(this);
                            m_playlistCollection.Add(addSong);
                            m_activePlaylist.AddLast(addSong);
                        }
                    }
                }

                if(m_shuffleLoad)
                {
                    Shuffle();
                }

                m_playlistFile = fileLocation;

                UpdatePlaylist();
                m_isValid = true;
                return;
            } 
        }

        private void CreateNew(string name, string fileLocation)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("Playlist");
            XElement info = new XElement("Info");
            XElement songs = new XElement("Songs");
            info.Add(new XAttribute("Name", name));
            info.Add(new XAttribute("Repeat", m_repeat));
            info.Add(new XAttribute("Shuffle", m_shuffleLoad));
            root.Add(info);
            root.Add(songs);
            doc.Add(root);
            doc.Save(fileLocation);

            m_name = name;

            m_playlistFile = fileLocation;
        }

        private void UpdatePlaylist()
        {
            if (m_saveable == false)
                return;

            string fileLocation = Path.Combine(Utils.DataFolder, m_owner.Id.ToString(), c_playlistFolder, $"{Name}.xml");
            if (!File.Exists(fileLocation))
            {
                CreateNew(Name, fileLocation);
            }

            XDocument doc = XDocument.Load(fileLocation);
            XElement root = doc.Root;
            XElement info = root.Element("Info");
            if (info.Attribute("Repeat") != null)
                info.Attribute("Repeat").Value = Repeat.ToString();
            else
                info.Add(new XAttribute("Repeat", Repeat));

            if (info.Attribute("Shuffle") != null)
                info.Attribute("Shuffle").Value = ShuffleLoad.ToString();
            else
                info.Add(new XAttribute("Shuffle", ShuffleLoad));

            if (info.Attribute("Volume") != null)
                info.Attribute("Volume").Value = Volume.ToString();
            else
                info.Add(new XAttribute("Volume", Volume));

            string xmlName = string.Empty;
            if (info.TryGetAttribute("Name", out xmlName))
            {
                if (xmlName != Name)
                {
                    Logging.LogError(LogType.Bot, $"Xml document {fileLocation} has info Name {xmlName} but Name {Name} was used for loading" +
                        " playlist in server {m_owner.Name} cant update playlist.");
                    m_isValid = false;
                    return;
                }

                XElement songs = root.Element("Songs");
                songs.RemoveAll();
                lock(m_playlistCollection)
                {
                    foreach(var song in m_playlistCollection)
                    {
                        XElement songElement = SetSongInfoToXml(song.SongInfo);
                        songs.Add(songElement);
                    }
                }

                doc.Save(fileLocation);
            }
        }

        private SongInfo GetSongInfoFromXml(XElement element)
        {
            SongInfo info;
            string title = string.Empty;
            if(!element.TryGetAttribute("Title", out title))
            {
                info = new SongInfo()
                {
                    Title = string.Empty
                };
                return info;
            }

            string uri = string.Empty;
            if (!element.TryGetAttribute("Uri", out uri))
            {
                info = new SongInfo()
                {
                    Title = string.Empty
                };
                return info;
            }

            string query = string.Empty;
            if (!element.TryGetAttribute("Query", out query))
            {
                info = new SongInfo()
                {
                    Title = string.Empty
                };
                return info;
            }

            string provider = string.Empty;
            if (!element.TryGetAttribute("Provider", out provider))
            {
                info = new SongInfo()
                {
                    Title = string.Empty
                };
                return info;
            }

            string providerTypeString = string.Empty;
            MusicType providerType = MusicType.Normal;
            if (!element.TryGetAttribute("ProviderType", out providerTypeString))
            {
                info = new SongInfo()
                {
                    Title = string.Empty
                };
                return info;
            }

            providerType = (MusicType)Enum.Parse(typeof(MusicType), providerTypeString);

            info = new SongInfo()
            {
                Title = title,
                Uri = uri,
                Query = query,
                Provider = provider,
                ProviderType = providerType
            };

            return info;
        }

        private XElement SetSongInfoToXml(SongInfo song)
        {
            if(string.IsNullOrEmpty(song.Title))
                return null;

            XElement newElement = new XElement("Song");
            XAttribute title    = new XAttribute("Title", song.Title);
            XAttribute uri      = new XAttribute("Uri", song.Uri);
            XAttribute query    = new XAttribute("Query", song.Query);
            XAttribute provider = new XAttribute("Provider", song.Provider);
            XAttribute providerType = new XAttribute("ProviderType", song.ProviderType);

            newElement.Add(title);
            newElement.Add(uri);
            newElement.Add(query);
            newElement.Add(provider);
            newElement.Add(providerType);

            return newElement;
        }

        #endregion
    }
}
