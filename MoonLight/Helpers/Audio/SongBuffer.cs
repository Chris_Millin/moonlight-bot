﻿using MoonLight.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoonLight.Helpers.Audio
{
    /// <summary>
    /// Create a buffer for a song file. It will create multiples files to ensure, that radio don't fill up disk space.
    /// It also help for large music by deleting files that are already seen.
    /// </summary>
    internal class SongBuffer
    {
        #region Public Members

        public bool BufferingCompleted
        {
            get;
            private set;
        } = false;

        public bool FailedToWrite
        {
            get;
            private set;
        } = false;

        #endregion

        #region Private Members

        private string      m_baseName;
        private SongInfo    m_songInfo;

        private int m_skipTo;
        private float m_volume = 0.5f;

        private long m_fileNumber = -1;
        private long m_nextFileId = 0;

        private ulong m_currentBufferSize = 0;
        private static int c_maxFileSize = 20.MiB();

        #endregion

        #region Public Methods

        public SongBuffer(string basename, SongInfo songInfo, int skipTo, float volume)
        {
            m_baseName = basename;
            m_songInfo = songInfo;
            m_skipTo = skipTo;
            m_volume = volume;
        }

        public Task BufferSong(CancellationToken cancelToken)
        {
            return Task.Factory.StartNew(async () =>
            {
                Process p = null;
                FileStream outStream = null;
                try
                {
                    p = Process.Start(new ProcessStartInfo
                    {
                        FileName = @"C:\Program Files\ffmpeg\bin\ffmpeg.exe",
                        Arguments = $"-ss {m_skipTo} -i \"{m_songInfo.Uri}\" -f s16le -ar 48000 -af \"volume = {m_volume}\" -ac 2 pipe:1 -loglevel quiet",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = false,
                        CreateNoWindow = false,
                    });

                    byte[] buffer = new byte[81920];
                    int currentFileSize = 0;
                    ulong prebufferSize = 100ul.MiB();

                    outStream = new FileStream(m_baseName + "-" + ++m_fileNumber, FileMode.Append, FileAccess.Write, FileShare.Read);
                    while (!p.HasExited) //Also fix low bandwidth
                   {
                        int bytesRead = await p.StandardOutput.BaseStream.ReadAsync(buffer, 0, buffer.Length, cancelToken).ConfigureAwait(false);
                        if (currentFileSize >= c_maxFileSize)
                        {
                            try
                            {
                                outStream.Dispose();
                            }
                            catch { }
                            outStream = new FileStream(m_baseName + "-" + ++m_fileNumber, FileMode.Append, FileAccess.Write, FileShare.Read);
                            currentFileSize = bytesRead;
                        }
                        else
                        {
                            currentFileSize += bytesRead;
                        }
                        m_currentBufferSize += Convert.ToUInt64(bytesRead);
                        await outStream.WriteAsync(buffer, 0, bytesRead, cancelToken).ConfigureAwait(false);
                        while (m_currentBufferSize > prebufferSize)
                            await Task.Delay(100, cancelToken);
                    }
                    if (currentFileSize == 0 && m_fileNumber == 0)
                    {
                        FailedToWrite = true;
                    }

                    BufferingCompleted = true;

                }
                catch (System.ComponentModel.Win32Exception ex)
                {
                    Logging.LogError(LogType.Bot, "FFMPEG may not be installed correctly");
                    Logging.LogException(LogType.Bot, ex, "Failed to run ffmpeg");
                }
                catch (Exception ex)
                {
                    Logging.LogException(LogType.Bot, ex, $"Buffering stopped");
                }
                finally
                {
                    if (outStream != null)
                        outStream.Dispose();
                    Logging.LogInfo(LogType.Bot, $"Buffering done.");
                    if (p != null)
                    {
                        try
                        {
                            p.Kill();
                        }
                        catch { }
                        p.Dispose();
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// Return the next file to read, and delete the old one
        /// </summary>
        /// <returns>Name of the file to read</returns>
        public string GetNextFile()
        {
            string filename = m_baseName + "-" + m_nextFileId;

            if (m_nextFileId != 0)
            {
                try
                {
                    m_currentBufferSize -= Convert.ToUInt64(new FileInfo(m_baseName + "-" + (m_nextFileId - 1)).Length);
                    File.Delete(m_baseName + "-" + (m_nextFileId - 1));
                }
                catch { }
            }
            m_nextFileId++;
            return filename;
        }

        public bool IsNextFileReady()
        {
            return m_nextFileId <= m_fileNumber;
        }

        public void CleanFiles()
        {
            for (long i = m_nextFileId - 1; i <= m_fileNumber; i++)
            {
                try
                {
                    File.Delete(m_baseName + "-" + i);
                }
                catch { }
            }
        }

        #endregion
    }
}