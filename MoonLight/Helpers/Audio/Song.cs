﻿using Discord.Audio;
using MoonLight.Helpers;
using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using VideoLibrary;

namespace MoonLight.Helpers.Audio
{
    public class SongInfo
    {
        public string       Title           { get; internal set; }
        public string       Query           { get; internal set; }
        public string       Uri             { get; internal set; }
        public string       Provider        { get; internal set; }
        public MusicType    ProviderType    { get; internal set; }
        
    }

    public class Song
    {
        #region Public Members

        public string Name
        { 
            get { return m_songInfo.Title; }
        }

        public string Source
        {
            get { return m_songInfo.Query; }
        }

        public string CurrentTime
        {
            get
            {
                var time = TimeSpan.FromSeconds(m_bytesSent / 3840 / 50);
                return $"{(int)time.TotalMinutes}:{time.Seconds}";
            }
        }

        public SongInfo SongInfo
        {
            get { return m_songInfo; }
        }

        public int SkipTo
        {
            get { return SkipTo; }
            set
            {
                m_skipTo = value;
                m_bytesSent = (ulong)m_skipTo * 3840 * 50;
            }
        }

        #endregion

        #region Private Members

        private SongInfo m_songInfo;
        private Playlist m_playlist;

        private int     m_skipTo    = 0;
        private ulong   m_bytesSent = 0;

        #endregion

        #region Public Methods

        public Song(SongInfo songInfo)
        {
            m_songInfo = songInfo;
        }

        public void SetPlaylist(Playlist playlist)
        {
            m_playlist = playlist;
        }

        public async Task Play(IAudioClient voiceClient, CancellationToken cancelToken)
        {
            var filename = Path.Combine(Utils.TempFolder, DateTime.Now.Epoch().ToString());

            SongBuffer sb = new SongBuffer(filename, SongInfo, m_skipTo, m_playlist.Volume);
            var bufferTask = sb.BufferSong(cancelToken).ConfigureAwait(false);

            var inStream = new FileStream(sb.GetNextFile(), FileMode.OpenOrCreate, FileAccess.Read, FileShare.Write);

            m_bytesSent = 0;

            try
            {
                var attempt = 0;

                var prebufferingTask = CheckPrebufferingAsync(inStream, sb, cancelToken);
                var sw = new Stopwatch();
                sw.Start();
                var t = await Task.WhenAny(prebufferingTask, Task.Delay(5000, cancelToken));
                if (t != prebufferingTask)
                {
                    Logging.LogInfo(LogType.Bot, $"Song {m_songInfo.Title} Prebuffering timed out or canceled. Cannot get any data from the stream. Sever {m_playlist.ServerOwner}");
                    return;
                }
                else if (prebufferingTask.IsCanceled)
                {
                    Logging.LogInfo(LogType.Bot, $"Song {m_songInfo.Title} Prebuffering timed out. Cannot get any data from the stream. Sever {m_playlist.ServerOwner}");
                    return;
                }
                else if(sb.FailedToWrite)
                {
                    Logging.LogWarn(LogType.Bot, $"Song {m_songInfo.Title} buffered with 0 bytes may be dead link will refresh. Sever {m_playlist.ServerOwner}");
                }
                sw.Stop();

                Logging.LogWarn(LogType.Bot, $"Song {m_songInfo.Title} prebuffering successfully completed in  {sw.Elapsed}. Sever {m_playlist.ServerOwner}");

                const int blockSize = 3840;
                byte[] buffer = new byte[blockSize];
                while (!cancelToken.IsCancellationRequested)
                {
                    var read = inStream.Read(buffer, 0, buffer.Length);
                    unchecked
                    {
                        m_bytesSent += (ulong)read;
                    }
                    if (read < blockSize)
                    {
                        if (sb.IsNextFileReady())
                        {
                            inStream.Dispose();
                            inStream = new FileStream(sb.GetNextFile(), FileMode.Open, FileAccess.Read, FileShare.Write);
                            read += inStream.Read(buffer, read, buffer.Length - read);
                            attempt = 0;
                        }
                        if (read == 0)
                        {
                            if (sb.BufferingCompleted)
                                break;
                            if (attempt++ == 20)
                            {
                                voiceClient.Wait();
                                m_playlist.Skip();
                                break;
                            }
                            else
                                await Task.Delay(100, cancelToken).ConfigureAwait(false);
                        }
                        else
                            attempt = 0;
                    }
                    else
                        attempt = 0;

                    while (m_playlist.Paused)
                    {
                        Logging.LogInfo(LogType.Bot, $"cancellation token null? {cancelToken == null}");
                        await Task.Delay(200, cancelToken).ConfigureAwait(false);
                    }

                    //buffer = AdjustVolume(buffer, m_playlist.Volume);
                    voiceClient.Send(buffer, 0, read);
                }
            }
            catch(Exception ex)
            {
                Logging.LogException(LogType.Bot, ex, "Failed to play song.");
            }
            finally
            {
                await bufferTask;
                await Task.Run(() => voiceClient.Clear());
                if (inStream != null)
                    inStream.Dispose();
                sb.CleanFiles();
            }
        }

        public override string ToString()
        {
            return m_songInfo.Title;
        }

        #endregion

        #region Private Methods

        private async Task CheckPrebufferingAsync(Stream inStream, SongBuffer sb, CancellationToken cancelToken)
        {
            while (!sb.BufferingCompleted && inStream.Length < 2.MiB())
            {
                await Task.Delay(100, cancelToken);
            }
            Logging.LogInfo(LogType.Bot, "Buffering successfull");
        }

        #endregion

        #region Volume (Too VM PC)
        /*public unsafe static byte[] AdjustVolume(byte[] audioSamples, float volume)
        {
            Contract.Requires(audioSamples != null);
            Contract.Requires(audioSamples.Length % 2 == 0);
            Contract.Requires(volume >= 0f && volume <= 1f);
            Contract.Assert(BitConverter.IsLittleEndian);

            if (Math.Abs(volume - 1f) < 0.0001f) return audioSamples;

            // 16-bit precision for the multiplication
            int volumeFixed = (int)Math.Round(volume * 65536d);

            int count = audioSamples.Length / 2;

            fixed (byte* srcBytes = audioSamples)
            {
                short* src = (short*)srcBytes;

                for (int i = count; i != 0; i--, src++)
                    *src = (short)(((*src) * volumeFixed) >> 16);
            }

            return audioSamples;
        }*/
        #endregion

    }
}