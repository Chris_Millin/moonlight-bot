﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MoonLight.Helpers
{
    [DebuggerDisplay("{items[0].id.playlistId}")]
    public class YoutubePlaylistSearch
    {
        public YtPlaylistItem[] items { get; set; }
    }
    public class YtPlaylistItem
    {
        public YtPlaylistId id { get; set; }
    }
    public class YtPlaylistId
    {
        public string kind { get; set; }
        public string playlistId { get; set; }
    }
    [DebuggerDisplay("{items[0].id.videoId}")]
    public class YoutubeVideoSearch
    {
        public YtVideoItem[] items { get; set; }
    }
    public class YtVideoItem
    {
        public YtVideoId id { get; set; }
    }
    public class YtVideoId
    {
        public string kind { get; set; }
        public string videoId { get; set; }
    }
    public class PlaylistItemsSearch
    {
        public string nextPageToken { get; set; }
        public PlaylistItem[] items { get; set; }
    }
    public class PlaylistItem
    {
        public YtVideoId contentDetails { get; set; }
    }

    public class SoundCloudVideo
    {
        public string Kind { get; set; } = "";
        public long Id { get; set; } = 0;
        public SoundCloudUser User { get; set; } = new SoundCloudUser();
        public string Title { get; set; } = "";
        [JsonIgnore]
        public string FullName => User.Name + " - " + Title;
        public bool Streamable { get; set; } = false;
        [JsonProperty("permalink_url")]
        public string TrackLink { get; set; } = "";
        [JsonIgnore]
        public string StreamLink => $"https://api.soundcloud.com/tracks/{Id}/stream?client_id={WebRequester.SoundCloudApiKey}";
    }
    public class SoundCloudUser
    {
        [Newtonsoft.Json.JsonProperty("username")]
        public string Name { get; set; }
    }

    public enum RequestHttpMethod
    {
        Get,
        Post
    }

    public class WebRequester : Singleton<WebRequester>
    {
        public static string SoundCloudApiKey = "2a0f89f3e02fee7e738a8c3876cca436";
        public static string GoogleApiKey = "AIzaSyDnQDgweHmwsDu5WL0V9vojWu4vHCyyvcY";

        private DateTime lastRefreshed = DateTime.MinValue;
        private string token { get; set; } = "";

        public bool CheckUri(string Uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Uri);
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    response.Close();
                    return true;
                }
                else
                {
                    response.Close();
                    return false;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

        public async Task<Stream> GetResponseStreamAsync(string url,
            IEnumerable<KeyValuePair<string, string>> headers = null, RequestHttpMethod method = RequestHttpMethod.Get)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException(nameof(url));
            var cl = new HttpClient();
            cl.DefaultRequestHeaders.Clear();
            switch (method)
            {
                case RequestHttpMethod.Get:
                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            cl.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                        }
                    }
                    return await cl.GetStreamAsync(url).ConfigureAwait(false);
                case RequestHttpMethod.Post:
                    FormUrlEncodedContent formContent = null;
                    if (headers != null)
                    {
                        formContent = new FormUrlEncodedContent(headers);
                    }
                    var message = await cl.PostAsync(url, formContent).ConfigureAwait(false);
                    return await message.Content.ReadAsStreamAsync().ConfigureAwait(false);
                default:
                    throw new NotImplementedException("That type of request is unsupported.");
            }
        }

        public async Task<string> GetResponseStringAsync(string url,
            IEnumerable<KeyValuePair<string, string>> headers = null,
            RequestHttpMethod method = RequestHttpMethod.Get)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException(nameof(url));
            var cl = new HttpClient();
            cl.DefaultRequestHeaders.Clear();
            switch (method)
            {
                case RequestHttpMethod.Get:
                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            cl.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                        }
                    }
                    return await cl.GetStringAsync(url).ConfigureAwait(false);
                case RequestHttpMethod.Post:
                    FormUrlEncodedContent formContent = null;
                    if (headers != null)
                    {
                        formContent = new FormUrlEncodedContent(headers);
                    }
                    var message = await cl.PostAsync(url, formContent).ConfigureAwait(false);
                    return await message.Content.ReadAsStringAsync().ConfigureAwait(false);
                default:
                    throw new NotImplementedException("That type of request is unsupported.");
            }
        }


        public async Task<IEnumerable<string>> FindYoutubeUrlByKeywords(string keywords)
        {
            if (string.IsNullOrWhiteSpace(keywords))
                throw new ArgumentNullException(nameof(keywords), "Query not specified.");
            if (keywords.Length > 150)
                throw new ArgumentException("Query is too long.");

            var match = new Regex(@"(?:list=)(?<id>[\da-zA-Z\-_]*)").Match(keywords);
            if (match.Success)
            {
                List<string> youtubeVideos = new List<string>();
                var videos = await GetVideoIDs(match.Groups["id"].Value, 200);
                foreach(string id in videos)
                {
                    youtubeVideos.Add($"https://www.youtube.com/watch?v={id}");
                }

                return youtubeVideos;
            }

            //maybe it is already a youtube url, in which case we will just extract the id and prepend it with youtube.com?v=
            match = new Regex(@"(?:youtu\\.be\\/|v=)(?<id>[\da-zA-Z\-_]*)").Match(keywords);
            if (match.Success)
            {
                return new List<string> { $"https://www.youtube.com/watch?v={match.Groups["id"].Value}" };
            }

            if (string.IsNullOrWhiteSpace(GoogleApiKey))
                throw new InvalidCredentialException("Google API Key is missing.");

            var response = await GetResponseStringAsync(
                                    $"https://www.googleapis.com/youtube/v3/search?" +
                                    $"part=snippet&maxResults=1" +
                                    $"&q={Uri.EscapeDataString(keywords)}" +
                                    $"&key={GoogleApiKey}").ConfigureAwait(false);
            JObject obj = JObject.Parse(response);

            var data = JsonConvert.DeserializeObject<YoutubeVideoSearch>(response);

            if (data.items.Length > 0)
            {
                var toReturn = "http://www.youtube.com/watch?v=" + data.items[0].id.videoId.ToString();
                return new List<string> { toReturn };
            }
            else
                return null;
        }

        public async Task<IEnumerable<string>> GetRelatedVideoIds(string id, int count = 1)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id));
            var match = new Regex("(?:youtu\\.be\\/|v=)(?<id>[\\da-zA-Z\\-_]*)").Match(id);
            if (match.Length > 1)
            {
                id = match.Groups["id"].Value;
            }
            var response = await GetResponseStringAsync(
                                    $"https://www.googleapis.com/youtube/v3/search?" +
                                    $"part=snippet&maxResults={count}&type=video" +
                                    $"&relatedToVideoId={id}" +
                                    $"&key={GoogleApiKey}").ConfigureAwait(false);
            JObject obj = JObject.Parse(response);

            var data = JsonConvert.DeserializeObject<YoutubeVideoSearch>(response);

            return data.items.Select(v => "http://www.youtube.com/watch?v=" + v.id.videoId);
        }

        public async Task<string> GetPlaylistIdByKeyword(string query)
        {
            if (string.IsNullOrWhiteSpace(GoogleApiKey))
                throw new ArgumentNullException(nameof(query));
            var match = new Regex("(?:youtu\\.be\\/|list=)(?<id>[\\da-zA-Z\\-_]*)").Match(query);
            if (match.Length > 1)
            {
                return match.Groups["id"].Value.ToString();
            }
            var link = "https://www.googleapis.com/youtube/v3/search?part=snippet" +
                        "&maxResults=1&type=playlist" +
                       $"&q={Uri.EscapeDataString(query)}" +
                       $"&key={GoogleApiKey}";

            var response = await GetResponseStringAsync(link).ConfigureAwait(false);
            var data = JsonConvert.DeserializeObject<YoutubePlaylistSearch>(response);
            JObject obj = JObject.Parse(response);

            return data.items.Length > 0 ? data.items[0].id.playlistId.ToString() : null;
        }

        public async Task<IEnumerable<string>> GetVideoIDs(string playlist, int number = 50)
        {
            if (string.IsNullOrWhiteSpace(GoogleApiKey))
            {
                throw new ArgumentNullException(nameof(playlist));
            }
            if (number < 1)
                throw new ArgumentOutOfRangeException();

            string nextPageToken = null;

            List<string> toReturn = new List<string>();

            do
            {
                var toGet = number > 50 ? 50 : number;
                number -= toGet;
                var link =
                    $"https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails" +
                    $"&maxResults={toGet}" +
                    $"&playlistId={playlist}" +
                    $"&key={GoogleApiKey}";
                if (!string.IsNullOrWhiteSpace(nextPageToken))
                    link += $"&pageToken={nextPageToken}";
                var response = await GetResponseStringAsync(link).ConfigureAwait(false);
                var data = await Task.Run(() => JsonConvert.DeserializeObject<PlaylistItemsSearch>(response)).ConfigureAwait(false);
                nextPageToken = data.nextPageToken;
                toReturn.AddRange(data.items.Select(i => i.contentDetails.videoId));
            } while (number > 0 && !string.IsNullOrWhiteSpace(nextPageToken));

            return toReturn;
        }

        public async Task<SoundCloudVideo> ResolveVideoAsync(string url)
        {
            string apiKey = WebRequester.SoundCloudApiKey;
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException(nameof(url));
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentNullException(nameof(apiKey));

            var response = await WebRequester.Instance.GetResponseStringAsync($"http://api.soundcloud.com/resolve?url={url}&client_id={apiKey}").ConfigureAwait(false);

            var responseObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SoundCloudVideo>(response);
            if (responseObj?.Kind != "track")
                throw new InvalidOperationException("Url is either not a track, or it doesn't exist.");

            return responseObj;
        }

        public bool IsSoundCloudLink(string url) =>
            System.Text.RegularExpressions.Regex.IsMatch(url, "(.*)(soundcloud.com|snd.sc)(.*)");

        internal async Task<SoundCloudVideo> GetVideoByQueryAsync(string query)
        {
            string apiKey = WebRequester.SoundCloudApiKey;
            if (string.IsNullOrWhiteSpace(query))
                throw new ArgumentNullException(nameof(query));
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentNullException(nameof(apiKey));

            var response = await WebRequester.Instance.GetResponseStringAsync($"http://api.soundcloud.com/tracks?q={Uri.EscapeDataString(query)}&client_id={apiKey}").ConfigureAwait(false);

            var responseObj = JsonConvert.DeserializeObject<SoundCloudVideo[]>(response).Where(s => s.Streamable).FirstOrDefault();
            if (responseObj?.Kind != "track")
                throw new InvalidOperationException("Query yielded no results.");

            return responseObj;
        }

        public async Task<string> ShortenUrl(string url)
        {
            if (string.IsNullOrWhiteSpace(GoogleApiKey)) return url;
            try
            {
                var httpWebRequest =
                    (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/urlshortener/v1/url?key=" +
                                                       GoogleApiKey);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync().ConfigureAwait(false)))
                {
                    var json = "{\"longUrl\":\"" + Uri.EscapeDataString(url) + "\"}";
                    streamWriter.Write(json);
                }

                var httpResponse = (await httpWebRequest.GetResponseAsync().ConfigureAwait(false)) as HttpWebResponse;
                var responseStream = httpResponse.GetResponseStream();
                using (var streamReader = new StreamReader(responseStream))
                {
                    var responseText = await streamReader.ReadToEndAsync().ConfigureAwait(false);
                    return Regex.Match(responseText, @"""id"": ?""(?<id>.+)""").Groups["id"].Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Shortening of this url failed: " + url);
                Console.WriteLine(ex.ToString());
                return url;
            }
        }
    }
}
