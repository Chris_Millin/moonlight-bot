﻿using Discord;
using MoonLight.Bot.LuaInterfaces;
using NLua;
using System;
using System.Collections.Specialized;

namespace MoonLight.Helpers
{
    public enum DiscordEventType
    {
        PrivateMessageRecieved,
        PrivateMessageDeleted,
        MessageRecieved,
        MessageDeleted,
        MessageEdited,
        Connected,
        RoleUpdated,
        RoleDeleted,
        MemeberAdded,
        MemeberRemoved,
        LeftVoiceChannel,
        UserSpeaking,
        UserTyping,
        UserUpdated,
        VoiceClientConnected,
        VoiceStateUpdated,
        URLUpdate
    }

    public struct DiscordEvent
    {
        public IDiscordRule Rule;
        public DiscordEventType Type;
        public LuaFunction Event;
    }

    public interface IDiscordRule
    {
        string Help { get; }

        bool IsEventSupported(DiscordEventType eventType);

        bool Validate(EventArgs e);

        ListDictionary BuildParameters(EventArgs e);
    }

    public class DiscordBasicRule : IDiscordRule
    {
        public string Help
        {
            get { return "";  }
        }

        public bool IsEventSupported(DiscordEventType eventType)
        {
            if(eventType != DiscordEventType.MessageRecieved || eventType != DiscordEventType.PrivateMessageRecieved)
            {
                return true;
            }

            return false;
        }

        public ListDictionary BuildParameters(EventArgs e)
        {
            return new ListDictionary();
        }

        public bool Validate(EventArgs e)
        {
            return true;
        }
    }

    public class DiscordMessageRule : IDiscordRule
    { 
        public string Command
        {
            get { return m_triggerCommand; }
            set { m_triggerCommand = value; }
        }

        public string Pattern
        {
            get { return m_pattern; }
        }

        public string Help
        {
            get { return string.Format("!{0} {1}: {2}\n", Command, Pattern, m_help); }
        }

        private string m_triggerCommand;
        private string m_pattern    = string.Empty;
        private string m_help       = string.Empty;
        private DiscordCommandLineArgs m_arguments;


        public DiscordMessageRule(string command, string help)
        {
            m_triggerCommand    = command;
            m_help              = help;
        }

        public DiscordMessageRule(string command, string pattern, string help)
        {
            m_triggerCommand    = command;
            m_pattern           = pattern;
            m_help              = help;
            m_arguments = new DiscordCommandLineArgs(command, pattern, help);
        }

        public bool IsEventSupported(DiscordEventType eventType)
        {
            if (eventType == DiscordEventType.MessageRecieved ||
                eventType == DiscordEventType.PrivateMessageRecieved)
            {
                return true;
            }

            return false;
        }

        public ListDictionary BuildParameters(EventArgs e)
        {
            if (e.GetType() == typeof(MessageEventArgs) && m_arguments != null)
            {
                return m_arguments.Match(((MessageEventArgs)e).Message.Text);
            }
            else
            {
                return new ListDictionary();
            }
        }

        public virtual bool Validate(EventArgs e)
        {
            bool valid = false;
            if (e.GetType() == typeof(MessageEventArgs))
            {
                valid = ValidateMessage((MessageEventArgs)e);
            }
            else
            {
                valid = false;
            }

            return valid;
        }

        public virtual bool ValidateMessage(MessageEventArgs e)
        {
            string content = e.Message.Text;
            return ValidateContent(content);
        }

        protected virtual bool ValidateContent(string content)
        {
            bool valid = false;
            try
            {
                
                if(m_arguments != null)
                {
                    return m_arguments.IsMatch(content);
                }
                else
                {
                    string paddedContent = content + " "; //add padding when arguments arent specified
                    return paddedContent.StartsWith("!" + Command + " ");
                }
            }
            catch
            {
                valid = false;
            }

            return valid;
        }

        
    }

    public class DiscordAdminMessageRule : DiscordMessageRule
    {
        private ulong m_adminId = 0;

        public DiscordAdminMessageRule(ulong adminId, string command, string help)
            : base(command, help)
        {
            m_adminId = adminId;
            
        }

        public DiscordAdminMessageRule(ulong adminId, string command, string pattern, string help)
            : base(command, pattern, help)
        {
            m_adminId = adminId;   
        }

        public override bool Validate(EventArgs e)
        {
            if(e is MessageEventArgs)
            {
                if(((MessageEventArgs)e).Message.User.Id != m_adminId)
                {
                    return false;
                }
                else
                {
                    return base.Validate(e);
                }
            }

            return false;           
        }
    }
}
