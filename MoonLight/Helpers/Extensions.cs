﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MoonLight.Helpers
{
    public static class ListDictionaryExtensions
    {
        public static object GetValue(this ListDictionary dic, string key)
        {
            if (dic.Contains(key))
                return dic[key];
            else
                return null;
        }

        public static T GetValue<T>(this ListDictionary dic, string key)
        {
            if (dic.Contains(key) && dic[key] is T)
                return (T)dic[key];
            else
                return default(T);
        }
    }

    public static class XmlExtensions
    {
        public static T GetAttributeOrDefualt<T>(this XElement element, string name)
        {
            if (!element.HasAttributes)
                return default(T);
            if (element.Attribute(name) == null)
                return default(T);

            string attribute = element.Attribute(name).Value;
            return (T)Convert.ChangeType(attribute, typeof(T));
        }

        public static bool TryGetAttribute<T>(this XElement element, string name, out T item)
        {
            if (!element.HasAttributes)
            { item = default(T); return false; }
            if (element.Attribute(name) == null)
            { item = default(T); return false; }

            string attribute = element.Attribute(name).Value;
            try
            {
                item = (T)Convert.ChangeType(attribute, typeof(T));
                return true;
            }
            catch
            {
                item = default(T);
                return false;
            }
        }
    }

    public static class EventArgsExtensions
    {
        public static bool TryCast<T>(this EventArgs e, out T casted)
        {
            try
            {
                casted = (T)Convert.ChangeType(e, typeof(T));
                return true;
            }
            catch
            {
                casted = default(T);
                return false;
            }
        }
    }

    public static class LinkedListExtensions
    {
        public static void Shuffle<T>(this LinkedList<T> list)
        {
            Random rand = new Random();

            for (LinkedListNode<T> n = list.First; n != null; n = n.Next)
            {
                T v = n.Value;
                if (rand.Next(0, 2) == 1)
                {
                    n.Value = list.Last.Value;
                    list.Last.Value = v;
                }
                else
                {
                    n.Value = list.First.Value;
                    list.First.Value = v;
                }
            }
        }
    }

    public static class NumericalExtensions
    {
        public static double Epoch(this DateTime dt) => dt.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        public static int KiB(this int value)   => value * 1024;
        public static int KB(this int value)    => value * 1000;

        public static int MiB(this int value)   => value.KiB() * 1024;
        public static int MB(this int value)    => value.KB() * 1000;

        public static int GiB(this int value)   => value.MiB() * 1024;
        public static int GB(this int value)    => value.MB() * 1000;

        public static ulong KiB(this ulong value)   => value * 1024;
        public static ulong KB(this ulong value)    => value * 1000;

        public static ulong MiB(this ulong value)   => value.KiB() * 1024;
        public static ulong MB(this ulong value)    => value.KB() * 1000;

        public static ulong GiB(this ulong value)   => value.MiB() * 1024;
        public static ulong GB(this ulong value)    => value.MB() * 1000;
    }
}
