﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Bot.LuaInterfaces
{
    public interface IScriptContainer
    {
        //void RegisterServer(DiscordServer server);
        void RegisterServer(int i);
    }

    public interface IScript
    {
        DiscordScriptContext ActiveClient { get; set; }

        void RegisterCommands();
    }
}
