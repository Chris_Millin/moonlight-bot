﻿using NLua;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Bot.LuaInterfaces
{
    /// <summary>
    /// This is a proxy class for a lua table instance class
    /// 
    /// To trick lua and nlua into have an overridable instancable object it 
    /// seems a middle man is going to need to handle some of the events
    /// 
    /// since its not possible to call the tables class methods explicitly from c# nlua
    /// there are static version which take the table instance object and then call its class
    /// functions. 
    /// </summary>
    public class LuaScriptProxy : IDisposable
    {
        public string ScriptName
        {
            get { return ScriptInfo?.Name; }
        }

        public string ScriptFile
        {
            get { return ScriptInfo?.FullPath; }
        }

        public LuaTable LuaInstance
        {
            get { return m_luaScriptInstance; }
        }

        public DiscordScriptContext ActiveContext
        {
            get { return m_scriptContext; }
        }

        public LuaScriptInfo ScriptInfo
        {
            get { return m_scriptInfo; }
        }

        private LuaTable m_luaScriptInstance;

        private LuaFunction m_scriptRegisterCommands;
        private LuaFunction m_scriptReload;
        private LuaFunction m_scriptDispose;

        private LuaScriptInfo m_scriptInfo;

        private DiscordScriptContext m_scriptContext;

        private Lua m_luaContext = new Lua();

        public LuaScriptProxy(LuaScriptInfo scriptInfo, DiscordScriptContext context, LuaTable instance, LuaFunction registerEvent, LuaFunction scriptReload, LuaFunction disposeEvent)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                m_scriptInfo = scriptInfo;
                m_scriptContext = context;
                if (m_scriptContext == null)
                    Logging.LogError(LogType.Bot, "Building script proxy with a null script context object. Script wont be able to function correctly.");

                m_luaScriptInstance         = instance;
                m_scriptRegisterCommands    = registerEvent;
                m_scriptReload              = scriptReload;
                m_scriptDispose             = disposeEvent;              
            }
        }

        public void RegisterCommands()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                try
                {
                    m_scriptRegisterCommands?.Call(this);
                }
                catch(Exception ex)
                {
                    Logging.LogException(LogType.Bot, ex, "Exception occured while registering commands");
                }
            }
        }

        public void RefreshScript()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                try
                {
                    m_scriptReload?.Call(this);
                }
                catch (Exception ex)
                {
                    Logging.LogException(LogType.Bot, ex, "Exception occured while reloading script");
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    try
                    {
                        m_scriptDispose?.Call(this);
                    }
                    catch (Exception ex)
                    {
                        Logging.LogException(LogType.Bot, ex, "Exception occured while disposing of script");
                    }

                    m_luaScriptInstance = null;

                    // TODO: dispose managed state (managed objects).
                }
               
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LuaScriptProxy() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
