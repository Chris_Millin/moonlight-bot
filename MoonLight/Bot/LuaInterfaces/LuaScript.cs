﻿using MoonLight.Helpers;
using NLua;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoonLight.Bot.LuaInterfaces
{
    /// <summary>
    /// The core component of a Discord Lua script
    /// </summary>
    public class LuaScript : IDisposable
    {
        #region Public Members
     
        public string Name
        {
            get { return Path.GetFileNameWithoutExtension(m_scriptFile); }
        }

        public string Script
        {
            get { return m_scriptFile; }
        }

        public bool Valid
        {
            get { return m_scriptValid; }
        }

        public DiscordServer Owner
        {
            get { return m_owner; }
        }

        #endregion

        #region Private Members

        private const string c_initaliseFunc    = "Initalise";
        private const string c_registerFunc     = "RegisterCommands";
        private const string c_disposeFunc      = "Dispose";

        private const int c_maxWaitTime = 60000;

        private string m_scriptFile = string.Empty;
        private bool m_scriptValid  = false;

        private DiscordServer m_owner;
        private Lua m_luaState;
        private LuaFunction m_scriptInitalise;
        private LuaFunction m_scriptRegisterCommands;
        private LuaFunction m_scriptDispose;

        private DiscordMessageRule m_helpEvent;

        private Dictionary<DiscordEventType, List<DiscordEvent>> m_commandEvents = new Dictionary<DiscordEventType, List<DiscordEvent>>();

        private object m_mutex = new object();
        #endregion

        #region Public Methods

        /// <summary>
        /// Constructor for the Script object
        /// </summary>
        /// <param name="file">Location of the script</param>
        /// <param name="owner">The server that owns this instance of the script</param>
        public LuaScript(string file, DiscordServer owner)
        {
            m_scriptFile    = file;
            m_owner         = owner;
        }

        /// <summary>
        /// Inisalises the script object
        /// and also registers the script with the main discord host
        /// </summary>
        public void Initalise()
        {
            m_luaState = new Lua();
            m_luaState.LoadCLRPackage();
            RegisterLuaFunctions();
            RegisterDiscordEvents();
            LoadScript();
            ScriptInitalise();
            ScriptRegister();
        }

        /// <summary>
        /// Reloads the script
        /// </summary>
        /// <returns></returns>
        public bool Reload()
        {
            Dispose();
            Initalise();
            return m_scriptValid;
        }

        /// <summary>
        /// Destorys the script
        /// </summary>
        public void Dispose()
        {
            UnregisterDiscordEvents();
            ScriptRelease();
            m_luaState.Dispose();
            m_luaState = null;
        }

        /// <summary>
        /// Get the help information about a specific command
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <returns>Help string of the command</returns>
        public string CommandHelp(string commandName)
        {
            List<string> commandHelp = new List<string>();
            foreach (var commandType in m_commandEvents)
            {
                foreach (var command in commandType.Value)
                {
                    if (command.Rule is DiscordMessageRule)
                    {
                        DiscordMessageRule rule = (DiscordMessageRule)command.Rule;
                        if (rule.Command == commandName)
                            commandHelp.Add(rule.Help);
                    }
                }
            }

            if (commandHelp.Count == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Join("\n", commandHelp);
            }
        }

        /// <summary>
        /// Get all the help information from the scripts registered commands
        /// </summary>
        /// <returns>Help informaiton</returns>
        public string CommandHelp()
        {
            List<string> commandHelp = new List<string>();
            foreach (var commandType in m_commandEvents)
            {
                foreach (var command in commandType.Value)
                {
                    if (command.Rule is DiscordMessageRule)
                    {
                        DiscordMessageRule rule = (DiscordMessageRule)command.Rule;
                        string commandString = string.Format("`!{0}`", rule.Command);
                        if (!commandHelp.Contains(commandString))
                            commandHelp.Add(commandString);
                    }
                }
            }

            if (commandHelp.Count == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Join("\n", commandHelp);
            }
        }

        /// <summary>
        /// To string override
        /// </summary>
        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Loads the script and obtains all the core functions that are required
        /// for the script to run. If anything fails the script valid
        /// is set to false.
        /// </summary>
        private void LoadScript()
        {
            using (DebugLogging log = new DebugLogging())
            {
                if(!File.Exists(m_scriptFile))
                {
                    m_scriptValid = false;
                    Logging.LogError(LogType.Script, $"Script file {m_scriptFile} does not exist so nothing to load");
                    return;
                }

                try
                {
                    m_luaState.DoFile(m_scriptFile);
                }
                catch (Exception ex)
                {
                    m_scriptValid = false;
                    Logging.LogException(LogType.Script, ex, $"Failed to load script file {m_scriptFile} for server {m_owner.Name}.");
                    return;
                }

                m_scriptInitalise = m_luaState.GetFunction(c_initaliseFunc);
                if (m_scriptInitalise == null)
                {
                    Logging.LogError(LogType.Script, $"Failed to find function {c_initaliseFunc} in script file {m_scriptFile}");
                    m_scriptValid = false;
                    return;
                }

                m_scriptRegisterCommands = m_luaState.GetFunction(c_registerFunc);
                if (m_scriptRegisterCommands == null)
                {
                    Logging.LogError(LogType.Script, $"Failed to find function {c_registerFunc} in script file {m_scriptFile}");
                    m_scriptValid = false;
                    return;
                }

                m_scriptDispose = m_luaState.GetFunction(c_disposeFunc);
                if (m_scriptInitalise == null)
                {
                    Logging.LogError(LogType.Script, $"Failed to find function {c_disposeFunc} in script file {m_scriptFile}");
                    m_scriptValid = false;
                    return;
                }

                Logging.LogInfo(LogType.Script, $"Loaded Script {Name} for server {Owner.Name} Successfully.");
                m_scriptValid = true;
            }
        }

        /// <summary>
        /// Registers function with lua
        /// </summary>
        private void RegisterLuaFunctions()
        {
            m_luaState["Context"] = this;
            m_luaState.RegisterFunction("RegisterCommand",          this, typeof(LuaScript).GetMethod("RegisterCommand", BindingFlags.NonPublic | BindingFlags.Instance));
            m_luaState.RegisterFunction("RegisterCommandPattern",   this, typeof(LuaScript).GetMethod("RegisterCommandPattern", BindingFlags.NonPublic | BindingFlags.Instance));

            m_luaState.RegisterFunction("RegisterCommandAdmin",         this, typeof(LuaScript).GetMethod("RegisterCommandAdmin", BindingFlags.NonPublic | BindingFlags.Instance));
            m_luaState.RegisterFunction("RegisterCommandPatternAdmin",  this, typeof(LuaScript).GetMethod("RegisterCommandPatternAdmin", BindingFlags.NonPublic | BindingFlags.Instance));
        }

        /// <summary>
        /// Registers discord events
        /// </summary>
        private void RegisterDiscordEvents()
        {
            if(m_helpEvent == null)
            {
                m_helpEvent = new DiscordMessageRule("help", "[<command>]", "Shows the help for command");
            }

            DiscordHost.Instance.Client.MessageReceived += ScriptHelpEvent;
            DiscordHost.Instance.Client.MessageReceived += Client_MessageSent;
        }

        /// <summary>
        /// Removes discord events
        /// </summary>
        private void UnregisterDiscordEvents()
        {
            DiscordHost.Instance.Client.MessageReceived -= ScriptHelpEvent;
            DiscordHost.Instance.Client.MessageReceived -= Client_MessageSent;
        }

        #region Discord Events

        private void Client_MessageSent(object sender, Discord.MessageEventArgs e)
        {
            Utils.Execute(async () =>
            {
                if (m_owner != null && m_owner.HasChannel(e.Channel.Id) && !e.Channel.IsPrivate)
                {
                    await InvokeEventAsync<Discord.MessageEventArgs>(DiscordEventType.MessageRecieved, e);
                }
                else if (m_owner != null && m_owner.HasChannel(e.Channel.Id) && e.Channel.IsPrivate)
                {
                    await InvokeEventAsync<Discord.MessageEventArgs>(DiscordEventType.PrivateMessageRecieved, e);
                }
            });
        }
        
        #endregion

        /// <summary>
        /// Registers a command function with the script
        /// </summary>
        /// <param name="type">Type of event to hook into</param>
        /// <param name="command">the command to look for</param>
        /// <param name="pattern">the pattern of the command</param>
        /// <param name="help">help information about the command</param>
        /// <param name="function">the hook function to lua</param>
        private void RegisterCommandPattern(DiscordEventType type, string command, string pattern, string help, LuaFunction function)
        {
            DiscordMessageRule rule     = new DiscordMessageRule(command, pattern, help);
            DiscordEvent discordEvent   = new DiscordEvent();
            discordEvent.Rule           = rule;
            discordEvent.Type           = type;
            discordEvent.Event          = function;
            AddDiscordEvent(discordEvent);
        }

        /// <summary>
        /// Registers a command function with the script
        /// </summary>
        /// <param name="type">Type of event to hook into</param>
        /// <param name="command">the command to look for</param>
        /// <param name="help">help information about the command</param>
        /// <param name="function">the hook function to lua</param>
        private void RegisterCommand(DiscordEventType type, string command, string help, LuaFunction function)
        {
            DiscordMessageRule rule = new DiscordMessageRule(command, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = type;
            discordEvent.Event = function;
            AddDiscordEvent(discordEvent);
        }

        /// <summary>
        /// Registers a command function with the script
        /// </summary>
        /// <param name="type">Type of event to hook into</param>
        /// <param name="command">the command to look for</param>
        /// <param name="pattern">the pattern of the command</param>
        /// <param name="help">help information about the command</param>
        /// <param name="function">the hook function to lua</param>
        private void RegisterCommandPatternAdmin(DiscordEventType type, string command, string pattern, string help, LuaFunction function)
        {
            ulong adminId = DiscordHost.Instance.AdminId;
            DiscordAdminMessageRule rule = new DiscordAdminMessageRule(adminId, command, pattern, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = type;
            discordEvent.Event = function;
            AddDiscordEvent(discordEvent);
        }

        /// <summary>
        /// Registers a command function with the script
        /// </summary>
        /// <param name="type">Type of event to hook into</param>
        /// <param name="command">the command to look for</param>
        /// <param name="pattern">the pattern of the command</param>
        /// <param name="help">help information about the command</param>
        /// <param name="function">the hook function to lua</param>
        private void RegisterCommandAdmin(DiscordEventType type, string command, string help, LuaFunction function)
        {
            ulong adminId = DiscordHost.Instance.AdminId;
            DiscordAdminMessageRule rule = new DiscordAdminMessageRule(adminId, command, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = type;
            discordEvent.Event = function;
            AddDiscordEvent(discordEvent);
        }

        /// <summary>
        /// Add the constructed discord event object
        /// </summary>
        /// <param name="eventAction">discord event object</param>
        private void AddDiscordEvent(DiscordEvent eventAction)
        {
            DiscordEventType type = eventAction.Type;
            if (m_commandEvents.ContainsKey(type))
            {
                if (m_commandEvents[type] == null)
                {
                    m_commandEvents[type] = new List<DiscordEvent>();
                    m_commandEvents[type].Add(eventAction);
                }
                else
                {
                    m_commandEvents[type].Add(eventAction);
                }
            }
            else
            {
                m_commandEvents.Add(type, new List<DiscordEvent>());
                m_commandEvents[type].Add(eventAction);
            }
        }

        /// <summary>
        /// Initalises the Lua Script
        /// </summary>
        private void ScriptInitalise()
        {
            if (m_scriptValid)
            {
                Lock();
                try
                {
                    m_scriptInitalise.Call();
                }
                catch (Exception ex)
                {
                    using (DebugLogging log = new DebugLogging())
                    {
                        Logging.LogException(LogType.Script, ex, $"Script {Name} failed to initalise on server {m_owner.Name}.");
                    }
                }
                Unlock();
            }
        }

        /// <summary>
        /// Call the lua function to register commands
        /// </summary>
        private void ScriptRegister()
        {
            if (m_scriptValid)
            {
                Lock();
                try
                {
                    m_scriptRegisterCommands.Call();
                }
                catch (Exception ex)
                {
                    using (DebugLogging log = new DebugLogging())
                    {
                        Logging.LogException(LogType.Script, ex, $"Script {Name} failed to register commands on server {m_owner.Name}.");
                    }
                }
                Unlock();
            }
        }

        /// <summary>
        /// Calls the release event
        /// </summary>
        private void ScriptRelease()
        {
            if (m_scriptValid)
            {
                Lock();
                try
                {
                    //async cancellation token support
                    m_commandEvents.Clear();
                    m_scriptDispose.Call();
                }
                catch (Exception ex)
                {
                    using (DebugLogging log = new DebugLogging())
                    {
                        Logging.LogException(LogType.Script, ex, $"Script {Name} failed to unregister on server {m_owner.Name}.");
                    }
                }
                Unlock();
            }
        }

        /// <summary>
        /// Provides a lock to lua and to c# side to
        /// prevent a thread in lua causing issues with another thread in c#
        /// 
        /// There is a minor sleep just to prevent lua from getting rushed
        /// it seems
        /// </summary>
        private void Lock()
        {
            Monitor.Enter(m_mutex);
            Thread.Sleep(5);
        }

        /// <summary>
        /// Provides an unlock to the lock mutex in both c# and lua
        /// </summary>
        private void Unlock()
        {
            Thread.Sleep(5);
            Monitor.Exit(m_mutex);
        }

        /// <summary>
        /// Invokes the registered lua command based on the message type
        /// </summary>
        /// <typeparam name="T">Discord Event Arg type</typeparam>
        /// <param name="eventType">the event enum to trigger</param>
        /// <param name="e">the event args</param>
        /// <returns>true if successful</returns>
        private void InvokeEvent<T>(DiscordEventType eventType, EventArgs e)
        {
            if (!m_scriptValid)
                return;

            T discordEventArgs = default(T);
            if (e.TryCast<T>(out discordEventArgs))
            {
                if (m_commandEvents.ContainsKey(eventType))
                {
                    foreach (var eventObject in m_commandEvents[eventType])
                    {
                        if (eventObject.Type == eventType &&
                            eventObject.Rule.IsEventSupported(eventObject.Type) &&
                            eventObject.Rule.Validate(e))
                        {
                            ListDictionary parameters = eventObject.Rule.BuildParameters(e);

                            System.Threading.ThreadPool.QueueUserWorkItem((object sender) =>
                            {
                                Lock();
                                try
                                {
                                    eventObject.Event.Call(parameters, discordEventArgs);
                                }
                                catch (Exception ex)
                                {
                                    using (DebugLogging log = new DebugLogging())
                                    {
                                        Logging.LogException(LogType.Script, ex, "Failed to run registered event.");
                                    }
                                }
                                Unlock();
                            });
                            
                        }
                    }
                }
            }

            return;
        }

        /// <summary>
        /// I
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventType"></param>
        /// <param name="e"></param>
        private Task InvokeEventAsync<T>(DiscordEventType eventType, EventArgs e)
        {
            if (!m_scriptValid)
                return Task.Delay(1);

            return Task.Factory.StartNew(() =>
            {
                T discordEventArgs = default(T);
                if (e.TryCast<T>(out discordEventArgs))
                {
                    if (m_commandEvents.ContainsKey(eventType))
                    {
                        foreach (var eventObject in m_commandEvents[eventType])
                        {
                            if (eventObject.Type == eventType &&
                                eventObject.Rule.IsEventSupported(eventObject.Type) &&
                                eventObject.Rule.Validate(e))
                            {
                                ListDictionary parameters = eventObject.Rule.BuildParameters(e);

                                Lock();
                                try
                                {
                                    eventObject.Event.Call(parameters, discordEventArgs);
                                }
                                catch (Exception ex)
                                {
                                    using (DebugLogging log = new DebugLogging())
                                    {
                                        Logging.LogException(LogType.Script, ex, "Failed to run registered event.");
                                    }
                                }
                                Unlock();
                            }
                        }
                    }

                }
            });
        }


        private void ScriptHelpEvent(object sender, Discord.MessageEventArgs e)
        {
            if(e.Server != null && e.Server == m_owner.Server)
                Utils.Execute(async () => await ScriptHelpAsync(e));
            else if(e.Server == null && m_owner.IsPrivate)
                Utils.Execute(async () => await ScriptHelpAsync(e));
        }

        private Task ScriptHelpAsync(Discord.MessageEventArgs e)
        {
            if (m_helpEvent.Validate(e))
            {
                return Task.Factory.StartNew(() =>
                {
                    ListDictionary parameters = m_helpEvent.BuildParameters(e);
                    if (parameters.Contains("<command>"))
                    {
                        string command = parameters["<command>"].ToString();
                        string message = CommandHelp(command);
                        if (!string.IsNullOrEmpty(message))
                        {
                            foreach (var part in Utils.SplitAndWrapString(message, '\n'))
                            {
                                e.Channel.SendMessage(string.Format("```{0}```", part));
                                Task.Delay(500).ConfigureAwait(false);
                            }
                        }
                    }
                    else
                    {
                        string message = CommandHelp();
                        if (!string.IsNullOrEmpty(message))
                        {
                            foreach (var part in Utils.SplitAndWrapString(message, '\n'))
                            {
                                e.Channel.SendMessage(part);
                                Task.Delay(500).ConfigureAwait(false);
                            }
                        }
                    }
                });
            }

            return Task.Delay(1);

        }
        #endregion
    }
}
