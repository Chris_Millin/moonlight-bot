﻿using MoonLight.Bot.LuaInterfaces;
using MoonLight.Helpers;
using NLua;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Bot
{
    public class DiscordScriptContext
    {


        private DiscordServer m_server;

        private Dictionary<DiscordEventType, List<DiscordEvent>> m_commandEvents = new Dictionary<DiscordEventType, List<DiscordEvent>>();

        private static object lua_mutex = new object();

        public DiscordScriptContext(DiscordServer server)
        {
            m_server = server;

            DiscordHost.Instance.Client.MessageReceived += Client_MessageReceived;
        }

        private void Client_MessageReceived(object sender, Discord.MessageEventArgs e)
        {
            if (m_server != null && m_server.HasChannel(e.Channel.Id) && e.Server != null)
            {
                InvokeEvent<Discord.MessageEventArgs>(DiscordEventType.MessageRecieved, e);
            }
            if (m_server != null && m_server.HasChannel(e.Channel.Id) && e.Server == null)
            {
                InvokeEvent<Discord.MessageEventArgs>(DiscordEventType.PrivateMessageRecieved, e);
            }
        }

        public void RegisterCommand(LuaScriptProxy scriptInstance, DiscordEventType type, string command, string pattern, string help, LuaFunction function)
        {
            DiscordMessageRule rule = new DiscordMessageRule(command, pattern, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = type;
            discordEvent.Event = function;
            discordEvent.ScriptInstance = scriptInstance;
            AddDiscordEvent(discordEvent);
        }

        public void RegisterCommand(LuaScriptProxy scriptInstance, DiscordEventType type, string command, string help, LuaFunction function)
        {
            DiscordMessageRule rule = new DiscordMessageRule(command, help);
            DiscordEvent discordEvent = new DiscordEvent();
            discordEvent.Rule = rule;
            discordEvent.Type = type;
            discordEvent.Event = function;
            discordEvent.ScriptInstance = scriptInstance;
            AddDiscordEvent(discordEvent);
        }

        public string[] GetCommandHelp(LuaScriptProxy scriptInstance)
        {
            List<string> commandHelp = new List<string>();
            foreach (var commandType in m_commandEvents)
            {
                foreach (var command in commandType.Value)
                {
                    if(scriptInstance.ScriptFile == command.ScriptInstance.ScriptFile)
                        commandHelp.Add(command.Rule.Help);
                }
            }

            return commandHelp.ToArray();
        }

        private void AddDiscordEvent(DiscordEvent eventAction)
        {
            DiscordEventType type = eventAction.Type;
            if (m_commandEvents.ContainsKey(type))
            {
                if (m_commandEvents[type] == null)
                {
                    m_commandEvents[type] = new List<DiscordEvent>();
                    m_commandEvents[type].Add(eventAction);
                }
                else
                {
                    m_commandEvents[type].Add(eventAction);
                }
            }
            else
            {
                m_commandEvents.Add(type, new List<DiscordEvent>());
                m_commandEvents[type].Add(eventAction);
            }
        }

        private bool InvokeEvent<T>(DiscordEventType eventType, EventArgs e)
        {
            T discordEventArgs = default(T);
            if (e.TryCast<T>(out discordEventArgs))
            {
                if (m_commandEvents.ContainsKey(eventType))
                {
                    bool invoked = false;
                    foreach (var eventObject in m_commandEvents[eventType])
                    {
                        if (eventObject.Type == eventType &&
                            eventObject.Rule.IsEventSupported(eventObject.Type) &&
                            eventObject.Rule.Validate(e))
                        {
                            invoked = true;

                            ListDictionary parameters = eventObject.Rule.BuildParameters(e);

                            System.Threading.ThreadPool.QueueUserWorkItem((object sender) =>
                            {
                                try
                                {
                                    lock(lua_mutex)
                                    {
                                        eventObject.Event.Call(eventObject.ScriptInstance, (ListDictionary)sender, discordEventArgs);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logging.LogException(LogType.Script, ex, "Failed to run registered event.");
                                }

                            }, parameters);
                        }

                    }

                    return invoked;
                }
            }

            return false;
        }
    }
}
