﻿using Discord;
using Discord.Audio;
using MoonLight.Bot.LuaInterfaces;
using MoonLight.Helpers;
using NLua;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MoonLight.Bot
{
    public class DiscordServer
    {
        #region public Members
        
        public static ulong PrivateId       { get { return 1; } }
        public static string PrivateName    { get { return "Private"; } }

        public string Name
        {
            get
            {
                if (!m_isPrivateServer)
                    return m_server.Name;
                else
                    return PrivateName;
            }
        }

        public ulong Id
        {
            get
            {
                if (!m_isPrivateServer)
                    return m_server.Id;
                else
                    return PrivateId;
            }
        }

        public string IdString
        {
            get { return Id.ToString(); }
        }

        public string Creator
        {
            get
            {
                if (!m_isPrivateServer)
                    return m_server.Owner.Name;
                else
                    return DiscordHost.Instance.Client.CurrentUser.Name;
            }
        }

        public bool IsPrivate
        {
            get { return m_isPrivateServer; }
        }

        public Server Server
        {
            get { return m_server; }
        }

        public IAudioClient AudioClient
        {
            get { return m_currentAudioClient; }
        }

        #endregion

        #region Private Members

        private bool m_isPrivateServer;
        private Server m_server;
        private List<LuaScript> m_scripts = new List<LuaScript>();

        private bool m_isVoiceConnected = false;

        private IAudioClient m_currentAudioClient;

        #endregion

        #region Public Methods

        public DiscordServer()
        {
            m_isPrivateServer = true;
            LoadScripts();
        }

        public DiscordServer(Server server)
        {
            m_server = server;
            LoadScripts();
        }

        public void LoadScripts()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                LoadGlobalScripts();
                LoadServerScripts();
            }
        }

        public void ReloadScripts()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                lock(m_scripts)
                {
                    foreach(var script in m_scripts)
                    {
                        script.Reload();
                    }
                }
            }
        }

        public void RemoveScripts()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                lock (m_scripts)
                {
                    foreach (var script in m_scripts)
                    {
                        script.Dispose();
                    }
                    m_scripts.Clear();
                }
            }
        }

        public bool LoadScript(string script)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                return LoadServerScript(script);
            }
        }

        public bool ReloadScript(string file)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                var script = m_scripts.FirstOrDefault(s => s.Script == file);
                if(script != null)
                {
                    script.Reload();
                    return true;
                }
                else
                {
                    Logging.LogError(LogType.Script, $"Cant find script {file} to reload");
                    return false;
                }
            }
        }

        public bool RemoveScript(string file)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                var script = m_scripts.FirstOrDefault(s => s.Script == file);
                if (script != null)
                {
                    m_scripts.Remove(script);
                    script.Dispose();
                    return true;
                }
                else
                {
                    Logging.LogError(LogType.Script, $"Cant find script {file} to reload");
                    return false;
                }
            }
        }

        public bool SendMessage(string message, string channel = "")
        {
            if (string.IsNullOrWhiteSpace(channel))
            {
                Channel discordChannel = m_server?.TextChannels.FirstOrDefault();
                if (discordChannel != null)
                {
                    discordChannel.SendMessage(message);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Channel discordChannel = m_server?.TextChannels.FirstOrDefault(c => c.Name == channel);
                if (discordChannel == null)
                    return SendMessage(message, channel);
                else
                    return false;
            }
        }

        public bool ConnectToVoice(string channel = "")
        {
            var voiceChannel = string.IsNullOrEmpty(channel) ? m_server.VoiceChannels.FirstOrDefault() : 
                m_server.VoiceChannels.FirstOrDefault(c => c.Name.Equals(channel, StringComparison.InvariantCultureIgnoreCase));
            if(voiceChannel == null)
            {
                string channelName = string.IsNullOrEmpty(channel) ? "FirstOrDefault" : channel;
                Logging.LogError(LogType.Bot, $"Failed to find voice channel {channelName} in server {Name}");
                return false;
            }
            else
            {
                string channelName = string.IsNullOrEmpty(channel) ? "FirstOrDefault" : channel;
                Logging.LogInfo(LogType.Bot, $"Found Channel {channelName} in server {Name} attempting to connect...");

                var service = DiscordHost.Instance.Client.GetService<AudioService>();
                ExecuteAndWait(async () =>
                {
                    m_currentAudioClient = await service.Join(voiceChannel);
                });
                
                if (m_currentAudioClient.State == ConnectionState.Connected || m_currentAudioClient.State == ConnectionState.Connecting)
                {
                    Logging.LogInfo(LogType.Bot, $"Successfully connected to Channel {channelName} in server {Name}.");
                    return true;
                }
                else
                {
                    Logging.LogError(LogType.Bot, $"Failed to connected to Channel {channelName} in server {Name}.");
                    return false;
                }
            }
        }

        public bool ChangeVoiceChannel(string channel = "")
        {
            string channelName = string.IsNullOrEmpty(channel) ? "FirstOrDefault" : channel;
            Logging.LogInfo(LogType.Bot, $"Attempting to change voice channel to {channelName} in server {Name}.");

            if (!DisconnectFromVoice())
                return false;

            Thread.Sleep(500); // stop is spamming the next command

            if (!ConnectToVoice(channel))
                return false;

            return true;
        }

        public bool DisconnectFromVoice()
        {
            if (m_currentAudioClient != null && (m_currentAudioClient.State != ConnectionState.Disconnected ||
                m_currentAudioClient.State != ConnectionState.Disconnecting))
            {
                Logging.LogInfo(LogType.Bot, $"Voice client in server {Name} attempting to disconnect...");
                ExecuteAndWait(async () =>
                {
                    await m_currentAudioClient.Disconnect();
                });

                if (m_currentAudioClient.State == ConnectionState.Disconnected || m_currentAudioClient.State == ConnectionState.Disconnecting)
                {
                    Logging.LogInfo(LogType.Bot, $"Successfully disconnected from voice in server {Name}.");
                    return true;
                }
                else
                {
                    Logging.LogError(LogType.Bot, $"Failed to Disconnect from voice in server {Name}.");
                    return false;
                }
            }
            else
            {
                Logging.LogInfo(LogType.Bot, $"Voice client in server {Name} is already disconnected.");
                return true;
            }
        }

        public bool HasScript(string file)
        {
            lock(m_scripts)
            {
                return m_scripts.FirstOrDefault(s => s.Script == file) != null;
            }
        }

        public bool HasChannel(ulong channelId)
        {
            if (m_isPrivateServer)
            {
                if (DiscordHost.Instance.Client.PrivateChannels.Count() == 0)
                    return false;

                return DiscordHost.Instance.Client.PrivateChannels.FirstOrDefault(c => c.Id == channelId) != null;
            }
            else
            {
                if (m_server.ChannelCount == 0)
                    return false;

                return m_server.GetChannel(channelId) != null;
            }
        }

        #endregion

        #region Private Methods

        private void LoadGlobalScripts()
        {
            string scriptFolder = Utils.GlobalScriptFolder;
            if (!Directory.Exists(scriptFolder))
            {
                Logging.LogError(LogType.Bot, $"Folder {scriptFolder} does not exist cant load any scripts");
                return;
            }

            foreach (var file in Directory.GetFiles(scriptFolder, "*.lua", SearchOption.AllDirectories))
            {
                LoadServerScript(file);
            }
        }

        private void LoadServerScripts()
        {
            string scriptFolder = Path.Combine(Utils.ScriptFolder, Id.ToString());
            if (!Directory.Exists(scriptFolder))
            {
                Logging.LogInfo(LogType.Bot, $"Folder {scriptFolder} does not exist cant load any scripts for server {Name}.");
                return;
            }

            foreach (var file in Directory.GetFiles(scriptFolder, "*.lua", SearchOption.AllDirectories))
            {
                LoadServerScript(file);
            }
        }

        private bool LoadServerScript(string file)
        {
            if (!File.Exists(file))
                return false;

            LuaScript script = new LuaScript(file, this);
            script.Initalise();
            if (script.Valid)
                m_scripts.Add(script);
            else
                return false;

            return true;
        }

        private void ExecuteAndWait(Func<Task> asyncAction)
        {
            asyncAction().GetAwaiter().GetResult();
        }

        #endregion

    }
}
