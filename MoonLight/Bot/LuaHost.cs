﻿using MoonLight.Bot.LuaInterfaces;
using MoonLight.Helpers;
using NLua;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Bot
{
    public class LuaScriptInfo
    {
        public string Name  { set; get; }
        public string FullPath  { set; get; }
        public ulong ServerId   { set; get; } //0 will be reserved as global

        public bool Equals(LuaScriptInfo y)
        {
            return Name.Equals(y.Name) &&
                FullPath.Equals(y.FullPath);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(LuaScriptInfo)) return false;
            return this.Equals((LuaScriptInfo)obj);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ FullPath.GetHashCode();
        }
    }

    public class LuaHost
    {
        

        public string ScriptFolder
        {
            get
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), c_scriptFolder);
            }
        }

        private const string c_scriptFolder         = "scripts";
        private const string c_globalScriptFolder   = "global";

        private Lua m_luaContext = new Lua();

        private Dictionary<LuaScriptInfo, LuaFunction> m_scriptContainers = new Dictionary<LuaScriptInfo, LuaFunction>();

        public LuaHost()
        {
            //load CLR into lua
            m_luaContext.LoadCLRPackage();
            m_luaContext.DebugHook += OnluaContextDebug;
        }

        public bool LoadScripts()
        {
            using (DebugLogging logger = new DebugLogging())
            {
                string scriptFolder = Path.Combine(ScriptFolder, c_globalScriptFolder);
                if (!Directory.Exists(scriptFolder))
                {
                    Logging.LogError(LogType.Bot, "Folder {0} does not exist cant load any scripts", scriptFolder);
                    return false;
                }

                foreach (var file in Directory.GetFiles(scriptFolder, "*.lua", SearchOption.AllDirectories))
                {
                    LoadFile(file);
                }
            }

            return true;
        }

        public bool LoadServerScript(DiscordServer server)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                string serverFolder = server.ServerId.ToString();
                string scriptFolder = Path.Combine(ScriptFolder, serverFolder);

                if (!Directory.Exists(scriptFolder))
                {
                    Logging.LogInfo(LogType.Bot, "Folder {0} does not exist cant load any scripts for server {1}.", scriptFolder, server.Name);
                    return false;
                }

                foreach (var file in Directory.GetFiles(scriptFolder, "*.lua", SearchOption.AllDirectories))
                {
                    LoadFile(file, server.ServerId);
                }

                return false;
            }
        }

        public bool LoadScript(string script, DiscordServer server = null)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                if(!File.Exists(script))
                {
                    Logging.LogError(LogType.Bot, "File {0} does not exists", script);
                    return false;
                }

                ulong id = 0;
                if (server != null)
                    id = server.ServerId;

                return LoadFile(script, id);
            }
        }


        public bool ReloadScript(LuaScriptInfo script)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                if (!File.Exists(script.FullPath))
                {
                    Logging.LogError(LogType.Bot, "File {0} does not exists", script.FullPath);
                    return false;
                }

                return LoadFile(script.FullPath, script.ServerId);
            }
        }

        public void RegisterServer(DiscordServer server)
        {
            LoadServerScript(server); 

            foreach(var scripts in m_scriptContainers)
            {
                if(scripts.Key.ServerId == 0 || scripts.Key.ServerId == server.ServerId)
                {
                    try
                    {
                        scripts.Value.Call(scripts.Key, server);
                    }
                    catch(Exception ex)
                    {
                        using (DebugLogging debug = new DebugLogging())
                        {
                            Logging.LogException(LogType.Bot, ex, "Could not create script instances");
                        }
                    }
                }
            }
        }

        private bool LoadFile(string file, ulong serverID = 0)
        {
            lock(m_luaContext)
            {
                try
                {
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    m_luaContext.DoFile(file);

                    var createFunc = m_luaContext.GetFunction(string.Format("create_{0}", fileName));
                    if (createFunc == null)
                    {
                        Logging.LogError(LogType.Bot, "Failed to get create_{0} function from Lua File {1}.", fileName, file);
                        return false;
                    }
                    else
                    {
                        Logging.LogInfo(LogType.Bot, "Found create_{0} function from Lua File {1}", fileName, file);
                        LuaScriptInfo info = new LuaScriptInfo();
                        info.Name = fileName;
                        info.FullPath = file;
                        info.ServerId = serverID;
                        if (!m_scriptContainers.ContainsKey(info))
                            m_scriptContainers.Add(info, createFunc);
                        else
                            Logging.LogError(LogType.Bot, "Failed to add new script create function. Has script {0} already been registered", file);
                    }

                    return true;
                }
                catch(Exception ex)
                {

                    Logging.LogException(LogType.Bot, ex, "Could not load Lua File");
                    return false;
                }
            }
        }

        private void RegisterStaticMethods()
        {
            m_luaContext.RegisterFunction("GetListValue", null, typeof(System.Console).GetMethod("WriteLine", new Type[] { typeof(String) }));
        }

        private void OnluaContextDebug(object sender, NLua.Event.DebugHookEventArgs e)
        {
            Logging.LogError(LogType.Lua, string.Format("Code {0}: {1} {2}", e.LuaDebug.eventCode, e.LuaDebug.namewhat, e.LuaDebug.shortsrc));
        }
    }
}

/*
    with tables the aim should be

    Load Script.
    get the default script table this only contains one method which is register server
    every time a server is loaded the lua script will pass a new table instance of the script
    the script will the register the normal functions and away it goes as normal.


*/