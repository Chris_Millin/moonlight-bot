﻿using Discord;
using Discord.Audio;
using MoonLight.Bot.LuaInterfaces;
using MoonLight.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MoonLight.Bot
{
    public class DiscordHost : Singleton<DiscordHost>
    {
        #region public members

        public DiscordClient Client
        {
            get { return m_client; }
        }

        public ulong AdminId
        {
            get { return m_adminId; }
        }

        public List<DiscordServer> Servers
        {
            get { return m_activeServers.Values.ToList(); }
        }

        public DateTime LastConnectTime
        {
            get;
            set;
        }

        public DateTime BotTime
        {
            get;
            set;
        } = DateTime.Now;

        #endregion

        #region private members

        private const int c_defaultReconnect = 500;

        private DiscordClient m_client;

        private ulong m_adminId = 0;
        private ulong m_serverChannelId = 0;

        private Dictionary<ulong, DiscordServer> m_activeServers = new Dictionary<ulong, DiscordServer>();

        private DateTime m_creationTime = DateTime.Now;

        private List<LuaScript> m_adminScripts = new List<LuaScript>();

        #endregion

        #region Public Methods

        public void Initalise(ListDictionary args)
        {
            if (!Directory.Exists(Utils.TempFolder))
                Directory.CreateDirectory(Utils.TempFolder);
            if (!Directory.Exists(Utils.DataFolder))
                Directory.CreateDirectory(Utils.DataFolder);

            ValueObject debugId = args.GetValue<ValueObject>("-debug");
            if (debugId != null && debugId.IsLong)
            {
                m_serverChannelId = (ulong)debugId.AsLong;
            }
            ValueObject adminId = args.GetValue<ValueObject>("-admin");
            if(adminId != null && adminId.IsLong)
            {
                m_adminId = (ulong)adminId.AsLong;
            }

            m_client = new DiscordClient((DiscordConfigBuilder config) =>
            {
                ValueObject arg = args.GetValue<ValueObject>("-name");
                if (arg != null)
                    config.AppName = arg.ToString();
                arg = args.GetValue<ValueObject>("-timeout");
                if (arg != null && arg.IsInt)
                    config.ReconnectDelay = arg.AsInt;
                else
                    config.ReconnectDelay = c_defaultReconnect;

                config.LogLevel = LogSeverity.Verbose;
                config.LogHandler += DiscordNetLogger;
            });

            m_client.UsingAudio((AudioServiceConfigBuilder config) =>
            {
                config.Bitrate  = 128;
                config.Mode     = AudioMode.Outgoing;
            });

            m_client.ServerAvailable    += OnServerOnline;
            m_client.ServerUnavailable  += OnServerDeleted;
            m_client.UserLeft           += OnUserLeftServer;
            m_client.Ready              += OnClientReady;
            m_client.GatewaySocket.Connected += OnGatewaySocketConnected;

            m_client.ExecuteAndWait(async () =>
            {
                ValueObject arg = args.GetValue<ValueObject>("-token");
                if (arg != null)
                    await m_client.Connect(arg.ToString(), TokenType.Bot);
            });
        }

        public void SetName(string name)
        {
            Utils.Execute(async () => await m_client.CurrentUser.Edit("", name));
        }

        public void SetAvatar(string location)
        {
            string userName = m_client.CurrentUser.Name;
            Utils.Execute(async () =>
            {
                using (WebClient client = new WebClient())
                {
                    Image image = Bitmap.FromStream(client.OpenRead(new Uri(location)));
                    Bitmap resized = new Bitmap(image, 200, 200);
                    using (MemoryStream stream = new MemoryStream(Utils.ImageToByteArray(resized)))
                    {
                        await m_client.CurrentUser.Edit("", userName, null, null, stream);
                    }
                }
            });
        }

        public bool AddScript(string script, string serverId = "")
        {
            using (DebugLogging logger = new DebugLogging())
            {
                ulong id = 0;
                if (!string.IsNullOrEmpty(serverId))
                    ulong.TryParse(serverId, out id);

                lock (m_activeServers)
                {
                    if (id == 0)
                    {
                        if (Utils.IsAdminScript(script))
                        {
                            Utils.Execute(async (token) => await LoadAdminScript(script), 1000);
                            return true;
                        }
                        else if (Utils.IsGlobalScript(script))
                        {
                            Utils.Execute(async (token) => await LoadGlobalScript(script), 1000);
                            return true;
                        }
                        else
                        {
                            Logging.LogError(LogType.Bot, $"Script {script} is not admin or global script and no server id was provided.");
                            return false;
                        }
                    }
                    else
                    {
                        if (!m_activeServers.ContainsKey(id))
                        {
                            Logging.LogError(LogType.Bot, $"Server with id {id} has not been registered with the bot so cant add script to it.");
                            return false;
                        }
                        else if (Utils.IsServerScript(script, id))
                        {
                            Utils.Execute(async (token) => await LoadServerScript(script, id), 1000);
                            return true;
                        }
                        else
                        {
                            Logging.LogError(LogType.Bot, $"Server Id {id} was passed to load the script into but either file is not saved to the script folder or server does not exist {script}.");
                            return false;
                        }
                    }
                }
            }
        }

        public bool UpdateScript(string script, string serverId = "")
        {
            using (DebugLogging logger = new DebugLogging())
            {
                ulong id = 0;
                if (!string.IsNullOrEmpty(serverId))
                    ulong.TryParse(serverId, out id);

                lock (m_activeServers)
                {
                    if (id == 0)
                    {
                        if (Utils.IsAdminScript(script))
                        {
                            Utils.Execute(async (token) => await ReloadAdminScript(script), 1000);
                            return true;
                        }
                        else if (Utils.IsGlobalScript(script))
                        {
                            Utils.Execute(async (token) => await ReloadGlobalScript(script), 1000);
                            return true;
                        }
                        else
                        {
                            Utils.Execute(async (token) => await ReloadGlobalScripts(), 1000);
                            return true;
                        }
                    }
                    else
                    {
                        if (!m_activeServers.ContainsKey(id))
                        {
                            Logging.LogError(LogType.Bot, $"Server with id {id} has not been registered with the bot so cant add script to it.");
                            return false;
                        }
                        else if(Utils.IsServerScript(script, id))
                        {
                            Utils.Execute(async (token) => await ReloadServerScript(script, id), 1000);
                            return true;
                        }
                        else
                        {
                            Logging.LogError(LogType.Bot, $"Server Id {id} was passed to reload the script but either file is not saved to the script folder or server does not exist {script}.");
                            return false;
                        }
                    }
                }
            }
        }

        public bool RemoveScript(string script, string serverId = "")
        {
            using (DebugLogging logger = new DebugLogging())
            {
                ulong id = 0;
                if (!string.IsNullOrEmpty(serverId))
                    ulong.TryParse(serverId, out id);

                lock (m_activeServers)
                {
                    if (id == 0)
                    {
                        if (Utils.IsAdminScript(script))
                        {
                            Utils.Execute(async (token) => await RemoveAdminScript(script), 1000);
                            return true;
                        }
                        else if (Utils.IsGlobalScript(script))
                        {
                            Utils.Execute(async (token) => await RemoveGlobalScript(script), 1000);
                            return true;
                        }
                        else
                        {
                            Logging.LogError(LogType.Bot, $"No server id provided and script is not admin or global script so cant remove script {script}.");
                            return false;
                        }
                    }
                    else
                    {
                        if (!m_activeServers.ContainsKey(id))
                        {
                            Logging.LogError(LogType.Bot, $"Server with id {id} has not been registered with the bot so cant remove script from it.");
                            return false;
                        }
                        else if (Utils.IsServerScript(script, id))
                        {
                            Utils.Execute(async (token) => await RemoveServerScript(script, id), 1000);
                            return true;
                        }
                        else
                        {
                            Logging.LogError(LogType.Bot, $"Server Id {id} was passed to remove the script but either file is not in the script folder or server does not exist {script}.");
                            return false;
                        }
                    }
                }
            }
        }

        public bool HasScript(string file)
        {
            lock (m_adminScripts)
            {
                return m_adminScripts.FirstOrDefault(s => s.Script == file) != null;
            }
        }

        #endregion

        #region Private Methods

        private Task LoadAdminScripts()
        {
            return Task.Factory.StartNew(() =>
            {
                using (DebugLogging logger = new DebugLogging())
                {
                    string scriptFolder = Utils.AdminScriptFolder;
                    if (!Directory.Exists(scriptFolder))
                    {
                        Logging.LogInfo(LogType.Bot, $"Folder {scriptFolder} does not exist cant load any admin scripts.");
                        return;
                    }

                    foreach (var file in Directory.GetFiles(scriptFolder, "*.lua", SearchOption.AllDirectories))
                    {
                        LuaScript script = new LuaScript(file, m_activeServers[DiscordServer.PrivateId]);
                        script.Initalise();
                        m_adminScripts.Add(script);
                    }
                }
            });
        }

        private Task<bool> LoadAdminScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
                if (HasScript(file))
                    return false;

                using (DebugLogging logger = new DebugLogging())
                {
                    if (!File.Exists(file))
                    {
                        Logging.LogInfo(LogType.Bot, $"File {file} does not exist cant load admin scripts.");
                        return false;
                    }
                    LuaScript script = new LuaScript(file, m_activeServers[DiscordServer.PrivateId]);
                    script.Initalise();
                    m_adminScripts.Add(script);
                    return true;
                }
            });
        }

        private Task<bool> LoadGlobalScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
               bool success = true;
               foreach (var server in m_activeServers)
               {
                   if (!server.Value.HasScript(file))
                   {
                       bool added = server.Value.LoadScript(file);
                       if (success != false)
                           success = added;
                   }
                   else
                   {
                       Logging.LogError(LogType.Bot, $"Server {server.Value.Name} already has script {file} added.");
                       success = false;
                   }
               }


               return success;
           });
        }

        private Task<bool> LoadServerScript(string file, ulong id)
        {
            return Task.Factory.StartNew(() =>
            {
                var server = m_activeServers[id];
                if (!server.HasScript(file))
                {
                    return server.LoadScript(file);
                }
                else
                {
                    Logging.LogError(LogType.Bot, $"Server {server.Name} already has script {file} added.");
                    return false;
                }
            });
        }

        private Task<bool> ReloadAdminScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
                using (DebugLogging logger = new DebugLogging())
                {
                    var script = m_adminScripts.FirstOrDefault(s => s.Script == file);
                    if (script != null)
                    {
                        script.Reload();
                        return true;
                    }
                    else
                    {
                        Logging.LogError(LogType.Script, $"Cant find script {file} to reload");
                        return false;
                    }
                }
            });
        }

        private Task<bool> ReloadGlobalScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
                bool success = true;
                foreach (var server in m_activeServers)
                {
                    if (server.Value.HasScript(file))
                    {
                        server.Value.ReloadScript(file);
                    }
                    else
                    {
                        if (success != false)
                            success = false;
                    }
                }

                return success;
            });
        }

        private Task<bool> ReloadGlobalScripts()
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var server in m_activeServers)
                {
                    server.Value.ReloadScripts();
                }

                return true;
            });
        }

        private Task<bool> ReloadServerScript(string file, ulong id)
        {
            return Task.Factory.StartNew(() =>
            {
                var server = m_activeServers[id];
                if (string.IsNullOrEmpty(file))
                {
                    server.ReloadScripts();
                    return true;
                }
                else
                {
                    if (server.HasScript(file))
                        return server.ReloadScript(file);
                    else
                        return false;
                }
            });
        }

        private Task<bool> RemoveAdminScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
                using (DebugLogging logger = new DebugLogging())
                {
                    var script = m_adminScripts.FirstOrDefault(s => s.Script == file);
                    if (script != null)
                    {
                        m_adminScripts.Remove(script);
                        script.Dispose();
                        return true;
                    }
                    else
                    {
                        Logging.LogError(LogType.Script, $"Cant find script {file} to reload");
                        return false;
                    }
                }
            });
        }

        private Task<bool> RemoveGlobalScript(string file)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var server in m_activeServers)
                {
                    if (server.Value.HasScript(file))
                    {
                        server.Value.RemoveScript(file);
                    }
                }

                return true;
            });
        }

        private Task<bool> RemoveGlobalScripts()
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var server in m_activeServers)
                {
                    server.Value.RemoveScripts();
                }

                return true;
            });
        }

        private Task<bool> RemoveServerScript(string file, ulong id)
        {
            return Task.Factory.StartNew(() =>
            {
                var server = m_activeServers[id];
                if (string.IsNullOrEmpty(file))
                {
                    server.RemoveScripts();
                    return true;
                }
                else
                {
                    if (server.HasScript(file))
                        return server.RemoveScript(file);
                    else
                        return false;
                }
            });
        }

        #region Discord Events

        private void OnUserLeftServer(object sender, UserEventArgs e)
        {
            if(e.User.Id == m_client.CurrentUser.Id)
            {
                lock(m_activeServers)
                {
                    if(m_activeServers.ContainsKey(e.Server.Id))
                    {
                        //m_activeServers[e.Server.Id].Dispose();
                        m_activeServers.Remove(e.Server.Id);
                    }
                }
            }
        }

        private void OnServerDeleted(object sender, ServerEventArgs e)
        {
            lock (m_activeServers)
            {
                if (m_activeServers.ContainsKey(e.Server.Id))
                {
                    //m_activeServers[e.Server.Id].Dispose();
                    m_activeServers.Remove(e.Server.Id);
                }
            }
        }

        private void OnServerOnline(object sender, ServerEventArgs e)
        {
            lock (m_activeServers)
            {
                if (!m_activeServers.ContainsKey(e.Server.Id))
                {
                    if(e.Server.Id == m_serverChannelId)
                        DebugLogging.DebugChannel = e.Server.DefaultChannel;
                    
                    m_activeServers.Add(e.Server.Id, new DiscordServer(e.Server));

                    
                }
            }
        }

        private void OnClientReady(object sender, EventArgs e)
        {
            lock (m_activeServers)
            {
                if (!m_activeServers.ContainsKey(DiscordServer.PrivateId))
                {
                    m_activeServers.Add(DiscordServer.PrivateId, new DiscordServer());
                    LoadAdminScripts();
                }
            }
        }

        private void OnGatewaySocketConnected(object sender, EventArgs e)
        {
            LastConnectTime = DateTime.Now;
        }

        private void DiscordNetLogger(object sender, LogMessageEventArgs arg)
        {
            using (DebugLogging logger = new DebugLogging())
            {
                if (arg.Exception != null && arg.Severity == LogSeverity.Error)
                    Logging.LogException(LogType.Discord, arg.Exception, arg.Message);
                else
                {
                    switch (arg.Severity)
                    {
                        case LogSeverity.Debug:
                            Logging.Log(LogType.Discord, LogLevel.Debug, arg.Message);
                            break;
                        case LogSeverity.Info:
                            Logging.LogInfo(LogType.Discord, arg.Message);
                            break;
                        case LogSeverity.Warning:
                            Logging.LogWarn(LogType.Discord, arg.Message);
                            break;
                        case LogSeverity.Error:
                            Logging.LogError(LogType.Discord, arg.Message);
                            break;
                        default:
                            Logging.Log(LogType.Discord, LogLevel.Debug, arg.Message);
                            break;
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}
