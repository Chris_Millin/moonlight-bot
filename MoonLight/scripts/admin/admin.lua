import('MoonLight', 'MoonLight.Bot')
import('MoonLight', 'MoonLight.Helpers')
import('Discord.Net', 'Discord')

DiscordEvents	= luanet.import_type('MoonLight.Helpers.DiscordEventType')
LogType			= luanet.import_type('MoonLight.LogType')
Logging			= luanet.import_type('MoonLight.Logging')
Utils 			= luanet.import_type('MoonLight.Helpers.Utils')
DiscordHost		= luanet.import_type('MoonLight.DiscordHost')

Path 			= luanet.import_type('System.IO.Path')
DateTime		= luanet.import_type('System.DateTime');

luanet.import_type('Discord.Message.Attahcment')
luanet.import_type('Discord.Message.File')

versionId = '0.4';

helpMsg = "\n`Admin Control v" .. versionId .. "`\nCommands:\n";

require 'helper'

--Core Functions
-----------------
function Initalise()
	Logging.LogInfo(LogType.Lua, 'Loading admin scripts v' .. versionId)
end

function RegisterCommands()
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "version", "\n    This Admin script contains controlls for the bot.\n	Admin Lua Script Version " .. versionId .. ".", Version)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "uptime [(bot | discord)]", "\n    How long the bot has been running for.", Uptime)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "change (name <name> | avatar | playing <text>)", "\n    Change info about bot (Attach image for avatar).", Change)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "leave <server>", "\n    Leave a server that the bot is a member of.", Leave)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "list (servers [<name> users] | scripts [<server>])", "\n    Leave a server that the bot is a member of.", List)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "message <message>", "\n    Messages a server or all servers.", Message)
end

function Dispose()

end
-----------------

--Event Functions
-----------------
function Version(sender, e)
	e.Channel:SendMessage("Admin script version " .. versionId)
end

function Uptime(sender, e)
	if sender:Contains("bot") then
		startTime = DiscordHost.Instance.BotTime
		info = FormatUptime(startTime)
		SendMessage(e, "Bot:\n" .. info)
	elseif sender:Contains("discord") then
		startTime = DiscordHost.Instance.LastConnectTime
		info = FormatUptime(startTime)
		SendMessage(e, "Discord:\n" .. info)
	else
		startTime = DiscordHost.Instance.BotTime
		info = "Bot:\n" .. FormatUptime(startTime)
		startTime = DiscordHost.Instance.LastConnectTime
		info = info .. "\nDiscord:\n" .. FormatUptime(startTime)
		SendMessage(e, info)
	end
end

function Change(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		DiscordHost.Instance:SetName(name)
	elseif sender:Contains("avatar") then
		if (e.Message.Attachments.Length == 1) then
			uri = e.Message.Attachments[0].Url
			DiscordHost.Instance:SetAvatar(uri)
		else
			e.Channel:SendMessage("No or too many Attachments provided cant change image");
		end
	end
end

function Leave(sender, e)
end

function List(sender, e)
	output = ""
	if sender:GetValue("servers") then
		if(sender:GetValue("users")) then
		else
			output = output .. "\n"
			output = output .. "```" .. "\n";
			servers = DiscordHost.Instance.Servers
			for i=0, servers.Count - 1 do
				output = output ..  "Name: " .. servers[i].Name .. "\nId: " .. servers[i].IdString .. "\nOwner: " .. servers[i].Creator .. "\n\n"
			end
			output = output .. "```" .. "\n";
		end
	end
	e.Channel:SendMessage(output)
end

function Message(sender, e)
	--[[server = ""
	if sender:Contains("<server>") then
		server = sender:Contains("<server>"):ToString()
		if sender:Contains("<message>") then
			message = sender:GetValue("<message>"):ToString()
			servers = DiscordHost.Instance.Servers
			for i=0,servers.Count-1 do
				if servers[i].Name == server then
					servers[i]:SendMessage(message)
					e.Channel:SendMessage("Sent message \n```" .. message .. "\n``` to " .. server .. " servers")
				end
			end
		end
	else]]--
	if sender:Contains("<message>") then
		message = sender:GetValue("<message>"):ToString()
		servers = DiscordHost.Instance.Servers
		for i=0,servers.Count-1 do
			servers[i]:SendMessage(message)
		end
		e.Channel:SendMessage("Sent message \n```" .. message .. "\n``` to all servers")
	end
	--end
end
-----------------

function FormatUptime(startTime)
	upTime = DateTime.Now - startTime
	info = "`Started: " .. startTime:ToString("dd/MM/yy HH:mm:ss") .. "`\n" ..
	"`Up Time: " .. upTime.Days .. " Days " .. upTime.Hours .. "h " .. upTime.Minutes .. "m " .. upTime.Seconds .. "s`"
	return info
end

function SendMessage(e, val)
	e.Channel:SendMessage(val)
end