import('MoonLight', 'MoonLight.Bot')
import('MoonLight', 'MoonLight.Helpers')
import('Discord.Net', 'Discord')

DiscordEvents	= luanet.import_type('MoonLight.Helpers.DiscordEventType')
LogType			= luanet.import_type('MoonLight.LogType')
Logging			= luanet.import_type('MoonLight.Logging')
Utils 			= luanet.import_type('MoonLight.Helpers.Utils')
DiscordHost		= luanet.import_type('MoonLight.DiscordHost')

Path 			= luanet.import_type('System.IO.Path')
Directory		= luanet.import_type('System.IO.Directory')
SearchOption	= luanet.import_type('System.IO.SearchOption')

luanet.import_type('Discord.Message.Attachment')
luanet.import_type('Discord.Message.File')

require 'helper'

helpMsg = "\n`Admin Script Manager!`\nCommands:\n";

--Core Functions
-----------------
function Initalise()
	Logging.LogInfo(LogType.Lua, 'Loading script manager v0.1')
end

function RegisterCommands()
	--RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script help", "\n    `Shows this message.`\n", Help)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script list [<server>]", "\n    `Lists all the stored scripts globally or for a specific server (server all will show global and all servers).`\n", ListScripts)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script get <name> [<server>]", "\n    `Sends the current specific script if it exits.`\n", GetScript)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script add [<server>]", "\n    `Add attached lua scripts to all servers or specific server.`\n", AddScript)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script update [<server>]", "\n    `Updates attached lua scripts to all servers or specific server if they already have it.`\n", UpdateScript)
	RegisterCommandPatternAdmin(DiscordEvents.PrivateMessageRecieved, "admin", "script remove <name> [<server>]", "\n    `Removes a script from the bot (will cause a restart if removed).`\n", RemoveScript)
end

function Dispose()

end
-----------------

--Event Functions
-----------------
function Help(sender, e)
	completeHelp = helpMsg
	
	helpMessages = Context:CommandHelp()

	for i=0, helpMessages.Count - 1  do
      completeHelp = completeHelp .. "\n" .. helpMessages[i] .. "\n"
    end
	
	e.Channel:SendMessage(completeHelp)
end

function ListScripts(sender, e)
	if sender:Contains("<server>") then

		fileLoc 	= Utils.ScriptFolder
		serverId = sender:GetValue("<server>"):ToString()
		if serverId == "all" then
		else
			fileLoc = Path.Combine(fileLoc, serverId)
			if Directory.Exists(fileLoc) then
				files = Directory.GetFiles(fileLoc, "*.lua", SearchOption.AllDirectories)
				message = ""
				for i = 0, files.Length-1 do
					message = message .. files[i] .. "\n"
				end
				e.Channel:SendMessage(message)
			else
				e.Channel:SendMessage("Folder path " .. fileLoc .. " doesnt exist.")
			end
		end
	else
		files = Directory.GetFiles(Utils.GlobalScriptFolder, "*.lua", SearchOption.AllDirectories)
		message = ""
		for i = 0, files.Length-1 do
			message = message .. files[i] .. "\n"
		end
		e.Channel:SendMessage(message)
	end
end

function GetScript(sender, e)
	if sender:Contains("<name>") then
			
		fileName 	= sender:GetValue("<name>"):ToString() .. ".lua"
		fileLoc 	= Utils.ScriptFolder

		if sender:Contains("<server>") then
			serverId = sender:GetValue("<server>"):ToString()
			fileLoc = Path.Combine(fileLoc, serverId)
			fileLoc = Path.Combine(fileLoc, fileName)

			if FileExists(fileLoc) then
				e.Channel:SendMessage("Sending file " .. fileLoc);
				e.Channel:SendFile(fileLoc)
			else
				e.Channel:SendMessage("No server file to send at location " .. fileLoc);
			end
		else
			fileLoc = Path.Combine(Utils.GlobalScriptFolder, fileName)
			if FileExists(fileLoc) then
				e.Channel:SendMessage("Sending file " .. fileLoc);
				e.Channel:SendFile(fileLoc)
			else
				e.Channel:SendMessage("No file to send at location " .. fileLoc);
			end
		end
	end

end

function AddScript(sender, e)
	if e.Message.Attachments.Length > 0  then
		for i=0,e.Message.Attachments.Length-1 do
			attachment 	= e.Message.Attachments[i]
			saveLoc 	= Utils.ScriptFolder

			if sender:Contains("<server>") then
				serverId = sender:GetValue("<server>"):ToString()
				saveLoc = Path.Combine(saveLoc, serverId)
				saveLoc = Path.Combine(saveLoc, attachment.Filename)

				if FileExists(saveLoc) then
					SendMessage(e, "File " .. saveLoc .. " already exists try updating rather than add")
					return
				end

			else
				saveLoc = Path.Combine(Utils.GlobalScriptFolder, attachment.Filename)
				if FileExists(saveLoc) then
					SendMessage(e, "File " .. saveLoc .. " already exists try updating rather than add")
					return
				end

			end

			SendMessage(e, "Downloading file " .. attachment.Url .. " to " .. saveLoc)

			success = Utils.DownloadFile(attachment.Url, saveLoc)
			if success == false then
				SendMessage(e, "Failed to download file " .. attachment.Url .. " to " .. saveLoc)
				return
			else

				SendMessage(e, "Adding script to servers.")

				if sender:Contains("<server>") then
					success = DiscordHost.Instance:AddScript(saveLoc, serverId)
				else
					success = DiscordHost.Instance:AddScript(saveLoc)
				end
				
				if success then
					SendMessage(e, "Added Script " .. attachment.Filename .. " to all servers");
				else
					SendMessage(e, "Failed to add script to one or more servers");
				end

				return
			end
			
		end
		
	else
		SendMessage(e, "No Attachment script provided.")
	end
end

function UpdateScript(sender, e)
	if e.Message.Attachments.Length > 0  then
		for i=0,e.Message.Attachments.Length-1 do
			attachment 	= e.Message.Attachments[i]
			saveLoc 	= Utils.ScriptFolder

			if sender:Contains("<server>") then
				serverId = sender:GetValue("<server>"):ToString()
				saveLoc = Path.Combine(saveLoc, serverId)
				saveLoc = Path.Combine(saveLoc, attachment.Filename)

				if FileExists(saveLoc) then
					os.remove(saveLoc)
				end

			else
				saveLoc = Path.Combine(Utils.GlobalScriptFolder, attachment.Filename)
				if FileExists(saveLoc) then
					os.remove(saveLoc)
				end

			end

			SendMessage(e, "Downloading file " .. attachment.Url .. " to " .. saveLoc)

			success = Utils.DownloadFile(attachment.Url, saveLoc)
			if success == false then
				SendMessage(e, "Failed to download file " .. attachment.Url .. " to " .. saveLoc)
				return
			else

				SendMessage(e, "updating script to servers.")

				if sender:Contains("<server>") then
					success = DiscordHost.Instance:UpdateScript(saveLoc, serverId)
				else
					success = DiscordHost.Instance:UpdateScript(saveLoc)
				end

				if success then
					SendMessage(e, "Added Script " .. attachment.Filename .. " to all servers");
				else
					SendMessage(e, "Failed to add script to one or more servers");
				end

				return
			end
			
		end
		
	else
		SendMessage(e, "No Attachment script provided reloading scripts any way.")
		DiscordHost.Instance:ReloadScript("")
	end
end

function RemoveScript(sender, e)
	if sender:Contains("<name>") then
			
		fileName 	= sender:GetValue("<name>"):ToString() .. ".lua"
		saveLoc 	= Utils.ScriptFolder

		if sender:Contains("<server>") then
			serverId = sender:GetValue("<server>"):ToString()
			saveLoc = Path.Combine(saveLoc, serverId)
			saveLoc = Path.Combine(saveLoc, fileName)

			if FileExists(saveLoc) then
				os.remove(saveLoc)
			end
		else
			saveLoc = Path.Combine(Utils.GlobalScriptFolder, fileName)
			if FileExists(saveLoc) then
				os.remove(saveLoc)
			end
		end

		SendMessage(e, "Removing script from servers.")

		if sender:Contains("<server>") then
			success = DiscordHost.Instance:RemoveScript(saveLoc, serverId)
		else
			success = DiscordHost.Instance:RemoveScript(saveLoc)
		end

		if success then
			SendMessage(e, "removed Script " .. fileName .. " to all servers");
		else
			SendMessage(e, "Failed to add script to one or more servers");
		end	
	else
		SendMessage(e, "No name specificed for the script so nothing to remove.")
	end
end
-----------------

function SendMessage(e, val)
	e.Channel:SendMessage(val)
end