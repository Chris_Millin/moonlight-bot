import('MoonLight', 'MoonLight.Bot')
import('Discord.Net', 'Discord')

DiscordEvents=luanet.import_type('MoonLight.Helpers.DiscordEventType')
LogType=luanet.import_type('MoonLight.LogType')
Logging=luanet.import_type('MoonLight.Logging')

AudioPlayer=luanet.import_type('MoonLight.Helpers.Audio.AudioPlayer')
MusicType=luanet.import_type('MoonLight.Helpers.Audio.MusicType')

luanet.import_type('MoonLight.Bot.LuaInterfaces.LuaScript')
luanet.import_type('MoonLight.Helpers.ValueObject')

serverAudioPlayer = nil
disposed = false

defaultPlaylist = "Default"

--Core Functions
-----------------
function Initalise()
	Logging.LogInfo(LogType.Lua, 'Initalising AudioPlayer v0.5')
end

function RegisterCommands()
	Logging.LogInfo(LogType.Lua, 'Registering commands.')

	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "help", "Shows this menu.", Help)

	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "connect [<name>]", "Connects to the voice channel <name> is optional.", VoiceConnect)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "reconnect", "Reconnects to the channel it was last connected to or refresh the connection.", VoiceReconnect)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "disconnect","Disconnect from the voice channel.", VoiceDisconnect)

	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "new <name>", "Creates a new playlist (will set it to current)", NewPlaylist)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "change [<name>]", "Changes the play list to another one. Leave name blank to change to default playlist", ChangePlaylist)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "delete <name>", "Deletes the specified playlist.", DeletePlaylist)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "info [last | current | next | <name>]", "Shows info about the current playlist.", PlaylistInfo)

	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "add <link>", "Adds a song to the current playlist.", Add)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "add playlist <name> <link>", "Adds a song to a specific playlist.", AddPlaylist)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "addnext <link>", "Adds a song to the next queue in the playlist.", Add)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "setnext <name>", "Sets the next song if the name is known.", Set)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "requeue", "Reqeues the entire playlist into the playing queue.", Queue)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "skip", "Skips the current playing song.", Skip)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "remove <name>", "Removes a clip from the playlist if the name is known.", Remove)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "clear [all]", "Clears the current playing if all is added it will clear the playlist of all known songs", Clear)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "shuffle [(enable | disable)]", "shuffles the curret playing list. Set enable or disable to shuffle on load/requeue", Shuffle)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "repeat (enable | disable)", "Enable or Disable Repeating", Repeat)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "(play | pause)", "Pause will pause the current playing and Play will start it again", Pause)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "volume <value>", "Changes the volume of the current playlist 0-100.", Volume)
	RegisterCommandPattern(DiscordEvents.MessageRecieved, "playlist", "stop", "Will stop the playlist", Stop)
end

function Dispose()
	
	if serverAudioPlayer == nil then
		disposed = true
	else
		serverAudioPlayer:Dispose()
    	disposed = true
		serverAudioPlayer = nil
	end

	Logging.LogInfo(LogType.Lua, 'AudioPlayer disposed of.')
end
-----------------

--Event Functions
-----------------
function Help(sender, e)
	helpmessage = Context:CommandHelp("playlist")
	e.Channel:SendMessage("__Playlist Help__\n\n```" .. helpmessage .. "```")
end

function VoiceConnect(sender, e)
	EnsureAudiPlayerRunning()

	if sender:Contains("<name>") then
		channelName = sender:GetValue("<name>"):ToString()
		e.Channel:SendMessage("Attempting to connect to voice channel " .. channelName)
		serverAudioPlayer:ConnectToVoice(channelName, e.Channel)
	else
		e.Channel:SendMessage("Attempting to connect to voice channel")
		serverAudioPlayer:ConnectToVoice("",  e.Channel)
	end
end

function VoiceReconnect(sender, e)
	EnsureAudiPlayerRunning()

	e.Channel:SendMessage("Reconnecting to voice channel")
	serverAudioPlayer:ReconnectToVoice(e.Channel)
end

function VoiceDisconnect(sender, e)

	e.Channel:SendMessage("Disconneting from voice channel")
	serverAudioPlayer:DisconnectFromVoice()
end


function NewPlaylist(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		if serverAudioPlayer:NewPlaylist(name, e.Channel) then
			return
		else
			e.Channel:SendMessage("Failed to create playlist.")
		end
	else
		e.Channel:SendMessage("No name provided to new playlist command.")
	end
end

function ChangePlaylist(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		if serverAudioPlayer:ChangePlaylist(name, e.Channel) then
			return
		else
			e.Channel:SendMessage("Failed to change playlist.")
		end
	else
		e.Channel:SendMessage("Changing to default playlists.")
		if serverAudioPlayer:ChangePlaylist(defaultPlaylist, e.Channel) then
			return
		else
			e.Channel:SendMessage("Failed to change playlist.")
		end
	end
end

function DeletePlaylist(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		if serverAudioPlayer:DeletePlaylist(name, e.Channel) then
			return
		else
			e.Channel:SendMessage("Failed to delete playlist.")
		end
	else
		e.Channel:SendMessage("No name provided to delete playlist command.")
	end
end

function PlaylistInfo(sender, e)
	if sender:Contains("last") then
		last = serverAudioPlayer.LastSong
		e.Channel:SendMessage("Last Played Song: " .. last)
	elseif sender:Contains("current") then
		current = serverAudioPlayer.CurrentSong
		e.Channel:SendMessage("Current Playing Song: " .. current)
	elseif sender:Contains("next") then
		nextsong = serverAudioPlayer.NextSong
		e.Channel:SendMessage("Next Song: " .. nextsong)
	elseif sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		info = serverAudioPlayer:GetPlaylistInfo(name)
		--e.Channel:SendMessage(info)
		SendLargeMessage(info, e.Channel)
	else
		info = serverAudioPlayer:GetPlaylistInfo()
		--e.Channel:SendMessage(info)
		SendLargeMessage(info, e.Channel)
	end
end

function Add(sender, e)
	if sender:Contains("add") then
		if sender:Contains("<link>") then
			link = sender:GetValue("<link>"):ToString()
			e.Channel:SendMessage("Adding song/s (if youtube playlist may take a while)")
			serverAudioPlayer:AddSong(link, MusicType.Normal, e.Channel)
		else
			e.Channel:SendMessage("No link provided to add")
			return
		end
	elseif sender:Contains("addnext") then
		if sender:Contains("<link>") then
			link = sender:GetValue("<link>"):ToString()
			e.Channel:SendMessage("Adding song/s (if youtube playlist may take a while)")
			serverAudioPlayer:AddNextSong(link, MusicType.Normal, e.Channel)
		else
			e.Channel:SendMessage("No link provided to addnext")
		end
	else
		e.Channel:SendMessage("Invalid Arguments to playlist add")
	end
end

function AddPlaylist(sender, e)
	if sender:Contains("add") then
		if sender:Contains("<link>") and sender:Contains("<name>") then
			link = sender:GetValue("<link>"):ToString()
			playlist = sender:GetValue("<name>"):ToString()
			e.Channel:SendMessage("Adding song/s (if youtube playlist may take a while)")
			serverAudioPlayer:AddSong(playlist, link, MusicType.Normal, e.Channel)
		else
			e.Channel:SendMessage("No link provided to add")
			return
		end
	elseif sender:Contains("addnext") then
		if sender:Contains("<link>") and sender:Contains("<name>")  then
			link = sender:GetValue("<link>"):ToString()
			playlist = sender:GetValue("<name>"):ToString()
			e.Channel:SendMessage("Adding song/s (if youtube playlist may take a while)")
			serverAudioPlayer:AddNextSong(playlist, link, MusicType.Normal, e.Channel)
		else
			e.Channel:SendMessage("No link provided to addnext")
		end
	else
		e.Channel:SendMessage("Invalid Arguments to playlist add")
	end
end

function Set(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		if serverAudioPlayer:SetNext(name, e.Channel) then
			e.Channel:SendMessage("Song " .. name .. " set to play next")
		else
			e.Channel:SendMessage("Failed to set next song to " .. name)
		end
	else
		e.Channel:SendMessage("No name provided to set to")
		return
	end
end

function Skip(sender, e)
	if serverAudioPlayer:Skip() == false then
		e.Channel:SendMessage("Cant Skip at the moment.")
	end
end

function Queue(sender, e)
	serverAudioPlayer:Requeue()
end

function Remove(sender, e)
	if sender:Contains("<name>") then
		name = sender:GetValue("<name>"):ToString()
		if serverAudioPlayer:Remove(name, e.Channel) then
			e.Channel:SendMessage("Remove song " .. name)
		else
			e.Channel:SendMessage("Failed to remove song " .. name)
		end
	else
		e.Channel:SendMessage("No name provided to remove")
		return
	end
end

function Clear(sender, e)
	if sender:Contains("all") then
		serverAudioPlayer:Clear(true, e.Channel)
		e.Channel:SendMessage("Playlist completely cleared")
	else
		serverAudioPlayer:Clear(false, e.Channel)
		e.Channel:SendMessage("Current playing queue cleared")
		return
	end
end

function Shuffle(sender, e)
	if sender:Contains("enable") then
		serverAudioPlayer.ShuffleLoad = true
		e.Channel:SendMessage("Repeat enabled")
	elseif sender:Contains("disable") then
		serverAudioPlayer.ShuffleLoad = false
		e.Channel:SendMessage("Repeat disabled")
	else
		serverAudioPlayer:Shuffle(e.Channel)
	end
end

function Volume(sender, e)
	if sender:Contains("<value>") then
		value = sender:GetValue("<value>"):ToString()
		e.Channel:SendMessage("Changing volume to " .. value .. " will change on next song");
		value = value / 100
		serverAudioPlayer.Volume = value

	else
		e.Channel:SendMessage("No value set for volume.")
	end
end

function Repeat(sender, e)
	if sender:Contains("enable") then
		serverAudioPlayer.Repeat = true
		e.Channel:SendMessage("Repeat enabled")
	elseif sender:Contains("disable") then
		serverAudioPlayer.Repeat = false
		e.Channel:SendMessage("Repeat disabled")
	end
end

function Pause(sender, e)
	if sender:Contains("pause") then
		serverAudioPlayer.Pause = true
		e.Channel:SendMessage("Paused Playlist")
	elseif sender:Contains("play") then
		serverAudioPlayer.Pause = false
		e.Channel:SendMessage("Un-paused Playlist")
	end
end

function Stop(sender, e)
	serverAudioPlayer:Stop()
end

function EnsureAudiPlayerRunning()
	if disposed == true then
		return
	end

	if serverAudioPlayer == nil then
		serverAudioPlayer = AudioPlayer(Context.Owner)
	end
end

function SendLargeMessage(message, channel)
	messageLenMax = 1950
	length = string.len(message)
	if (length < messageLenMax) then
		channel:SendMessage("```" .. message .. "```")
		return
	end
	startChar = 0
	endChar = messageLenMax;
	while startChar < length do
		subMsg = string.sub(message, startChar, endChar)
		subMsg = "```" .. subMsg .. "```"
		channel:SendMessage(subMsg)
		
		startChar = startChar + (endChar + 1);
		if ((length  -  startChar) >  messageLenMax) then
			endChar = endChar + messageLenMax
		else
			endChar = length
		end
	end
end
-----------------